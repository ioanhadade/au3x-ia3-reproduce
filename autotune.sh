#!/bin/bash

# Timothy M. Jones and Ioan C. Hadade

# simple script to search through prefetching distances.

# declare it as associative
declare -A var

# set number of runs
numruns=1
# specify log file to output results
log="autotune.dat"
# specify path to binary
app='./au3x-ia3-tune.exe'

if [ -z "$1" ]; then
    echo "Usage: ./autotune.sh [ARCH] where ARCH is: SNB, BDW, SKX, KNL or KNC"
    echo "Example: ./autotune.sh SKX"
    exit 0
fi

cmd="$1"
if   [ $cmd = "SNB" ]; then
    target="ARCH=-mavx"
    echo "Sandy Bridge uarch"
elif [ $cmd = "BDW" ]; then
    target="ARCH=-xCORE-AVX2"
    echo "Broadwell uarch"
elif [ $cmd = "SKX" ]; then
    target="ARCH=-xCORE-AVX512"
    echo "Skylake Server uarch"
elif [ $cmd = "KNL" ]; then
    target="-xMIC-AVX512"
    echo "Knights Landing uarch"
elif [ $cmd = "KNC" ]; then
    target="-mmic"
    echo "Knights Corner uarch"
else
    echo "Usage: ./autotune.sh [ARCH] where ARCH is: SNB, BDW, SKX, KNL or KNC"
    echo "Example: ./autotune.sh SKX"
    exit 0
fi


# run the binary and average results.
function run {
    for i in $(seq $numruns)
    do
        val=$($app | grep "Average (s)" | tr -s ' ' | cut -d " " -f 3)
    done
    echo "$val"
}

function build {
    cd src/
    make clean
    make VER=tune "$target"
    cd .. 
}

# baseline - disable software prefetching
unset ENABLE_PF
unset L1_INDEX_PF
unset L2_INDEX_PF
build
base=$(run)
echo "L1 off L2 off $base"

# start software prefetching
export ENABLE_PF=1
# Vary L1 distance
for i in 8 16 32 64 128 256
do
    # L1 only
    export L1_INDEX_PF=$i
    export L2_INDEX_PF=0
    build
    var[$i,0]=$(run)
    echo "L1 $i L2 off ${var[$i,0]}"

    # L2 only
    export L1_INDEX_PF=0
    export L2_INDEX_PF=$i
    build
    var[0,$i]=$(run)
    echo "L1 off L2 $i ${var[0,$i]}"
    
    # L1 and L2
    export L1_INDEX_PF=$i
    for j in $((i<<1)) $((i<<2)) $((i<<3))
    do
        export L2_INDEX_PF=$j
        build
        var[$i,$j]=$(run)
        echo "L1 $i L2 $j ${var[$i,$j]}"
    done
done

# output results statistics
echo "Speedup L1PF L2PF" > $log
for i in 8 16 32 64 128 256
do
    resl1=${var[$i,0]}
    echo -e "$(echo "scale=3; $base / $resl1" | bc) \t L1 $i L2 off" >> $log
    resl2=${var[0,$i]}
    echo -e "$(echo "scale=3; $base / $resl2" | bc) \t L1 off L2 $i" >> $log
    
    for j in $((i<<1)) $((i<<2)) $((i<<3))
    do
        resl1l2=${var[$i,$j]}
        echo -e "$(echo "scale=3; $base / $resl1l2" | bc) \t L1 $i L2 $j" >> $log
    done
done
