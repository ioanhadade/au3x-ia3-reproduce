# ifndef __VEC256_H__
# define __VEC256_H__


inline __attribute__((always_inline)) void
load(double *data[], int nvar, int offset, double out[][VECLEN])
{
   int ivar;
   __m256d v;

   for( ivar=0;ivar<nvar;ivar++ )
   {
      v= _mm256_load_pd(&data[ivar][offset]);
      _mm256_store_pd(&out[ivar][0],v);
   }
}

inline __attribute__((always_inline)) void
load(double *data[], int offset, double out[VECLEN])
{
    __m256d v;

    v= _mm256_load_pd(&data[0][offset]);
    _mm256_store_pd(&out[0],v);

}

template < typename type > inline __attribute__((always_inline)) 
void gather1x7(type *data, int *pos, double out[][VECLEN])
{
    // a0,b0,c0,d0
    __m256d v0 = _mm256_load_pd(&data[pos[0]].var[0]);
    // e0,f0,g0,h0
    __m256d va = _mm256_load_pd(&data[pos[0]].var[4]);
    // a1,b1,c1,d1
    __m256d v1 = _mm256_load_pd(&data[pos[1]].var[0]);
    // e1,f1,g1,h1
    __m256d vb = _mm256_load_pd(&data[pos[1]].var[4]);
    // a2,b2,c2,d2
    __m256d v2 = _mm256_load_pd(&data[pos[2]].var[0]);
    // e2,f2,g2,h2
    __m256d vc = _mm256_load_pd(&data[pos[2]].var[4]);
    // a3,b3,c3,d3
    __m256d v3 = _mm256_load_pd(&data[pos[3]].var[0]);
    // e3,f3,g3,h3
    __m256d vd = _mm256_load_pd(&data[pos[3]].var[4]);

    // 64-bit wide permutations
    // a0,a1,c0,c1
    __m256d shufl0 = _mm256_shuffle_pd(v0,v1,0x0);
    // e0,e1,g0,g1
    __m256d shufla = _mm256_shuffle_pd(va,vb,0x0);
    // b0,b1,d0,d1
    __m256d shufl1 = _mm256_shuffle_pd(v0,v1,0xF);
    // f0,f1,h0,h1
    __m256d shuflb = _mm256_shuffle_pd(va,vb,0xF);
    // a2,a3,c2,c3
    __m256d shufl2 = _mm256_shuffle_pd(v2,v3,0x0);
    // e2,e3,g2,g3
    __m256d shuflc = _mm256_shuffle_pd(vc,vd,0x0);
    // b2,b3,d2,d3
    __m256d shufl3 = _mm256_shuffle_pd(v2,v3,0xF);
    // f2,f3,h2,h3
    __m256d shufld = _mm256_shuffle_pd(vc,vd,0xF);

    // 128-bit wide permutations and store
    // a0,a1,a2,3
    _mm256_store_pd(&out[0][0],_mm256_permute2f128_pd(shufl0,shufl2,0x20));
    // b0,b1,b2,b3
    _mm256_store_pd(&out[1][0],_mm256_permute2f128_pd(shufl1,shufl3,0x20));
    // c0,c1,c2,c3
    _mm256_store_pd(&out[2][0],_mm256_permute2f128_pd(shufl0,shufl2,0x31));
    // d0,d1,d2,d3
    _mm256_store_pd(&out[3][0],_mm256_permute2f128_pd(shufl1,shufl3,0x31));
    // e0,e1,e2,e3
    _mm256_store_pd(&out[4][0],_mm256_permute2f128_pd(shufla,shuflc,0x20));
    // f0,f1,f2,f3
    _mm256_store_pd(&out[5][0],_mm256_permute2f128_pd(shuflb,shufld,0x20));
    // g0,g1,g2,g3
    _mm256_store_pd(&out[6][0],_mm256_permute2f128_pd(shufla,shuflc,0x31));
    // h0,h1,h2,h3 -- not required
}


template < typename type > inline __attribute__((always_inline)) 
void gather1x3(type *data, int *pos, double out[][VECLEN])
{
    __m256d v[4];
    // a0,b0,c0,d0
    v[0] = _mm256_load_pd(&data[pos[0]].var[0]);
    // a1,b1,c1,d1
    v[1] = _mm256_load_pd(&data[pos[1]].var[0]);
    // a2,b2,c2,d2
    v[2] = _mm256_load_pd(&data[pos[2]].var[0]);
    // a3,b3,c3,d3
    v[3] = _mm256_load_pd(&data[pos[3]].var[0]);

    // 64-bit wide permutations
    // a0,a1,c0,c1
    __m256d shufl0 = _mm256_shuffle_pd(v[0],v[1],0x0);
    // b0,b1,d0,d1
    __m256d shufl1 = _mm256_shuffle_pd(v[0],v[1],0xF);
    // a2,a3,c2,c3
    __m256d shufl2 = _mm256_shuffle_pd(v[2],v[3],0x0);
    // b2,b3,d2,d3
    __m256d shufl3 = _mm256_shuffle_pd(v[2],v[3],0xF);

    // 128-bit wide permutations and store
    // a0,a1,a2,a3
    _mm256_store_pd(&out[0][0],_mm256_permute2f128_pd(shufl0,shufl2,0x20));
    // b0,b1,b2,b3
    _mm256_store_pd(&out[1][0],_mm256_permute2f128_pd(shufl1,shufl3,0x20));
    // c0,c1,c2,c3
    _mm256_store_pd(&out[2][0],_mm256_permute2f128_pd(shufl0,shufl2,0x31));
    // d0,d1,d2,d3 -- not required
}

template < typename type > inline __attribute__((always_inline)) 
void gather3x7(type *data, int *pos, double out[][3][VECLEN])
{
    __m256d v0,v1,v2,v3;
    __m256d va,vb,vc,vd;
    __m256d shufl0,shufl1,shufl2,shufl3;
    __m256d shufla,shuflb,shuflc,shufld;

    // 1
    // a0,b0,c0,d0
    v0 = _mm256_load_pd(&data[pos[0]].var[0][0]);
    // e0,f0,g0,h0
    va = _mm256_load_pd(&data[pos[0]].var[0][4]);
    // a1,b1,c1,d1
    v1 = _mm256_load_pd(&data[pos[1]].var[0][0]);
    // e1,f1,g1,h1
    vb = _mm256_load_pd(&data[pos[1]].var[0][4]);
    // a2,b2,c2,d2
    v2 = _mm256_load_pd(&data[pos[2]].var[0][0]);
    // e2,f2,g2,h2
    vc = _mm256_load_pd(&data[pos[2]].var[0][4]);
    // a3,b3,c3,d3
    v3 = _mm256_load_pd(&data[pos[3]].var[0][0]);
    // e3,f3,g3,h3
    vd = _mm256_load_pd(&data[pos[3]].var[0][4]);

    // 64-bit wide permutations
    // a0,a1,c0,c1
    shufl0 = _mm256_shuffle_pd(v0,v1,0x0);
    // e0,e1,g0,g1
    shufla = _mm256_shuffle_pd(va,vb,0x0);
    // b0,b1,d0,d1
    shufl1 = _mm256_shuffle_pd(v0,v1,0xF);
    // f0,f1,h0,h1
    shuflb = _mm256_shuffle_pd(va,vb,0xF);
    // a2,a3,c2,c3
    shufl2 = _mm256_shuffle_pd(v2,v3,0x0);
    // e2,e3,g2,g3
    shuflc = _mm256_shuffle_pd(vc,vd,0x0);
    // b2,b3,d2,d3
    shufl3 = _mm256_shuffle_pd(v2,v3,0xF);
    // f2,f3,h2,h3
    shufld = _mm256_shuffle_pd(vc,vd,0xF);

    // 128-bit wide permutations and store
    // a0,a1,a2,3
    _mm256_store_pd(&out[0][0][0],_mm256_permute2f128_pd(shufl0,shufl2,0x20));
    // b0,b1,b2,b3
    _mm256_store_pd(&out[1][0][0],_mm256_permute2f128_pd(shufl1,shufl3,0x20));
    // c0,c1,c2,c3
    _mm256_store_pd(&out[2][0][0],_mm256_permute2f128_pd(shufl0,shufl2,0x31));
    // d0,d1,d2,d3
    _mm256_store_pd(&out[3][0][0],_mm256_permute2f128_pd(shufl1,shufl3,0x31));
    // e0,e1,e2,e3
    _mm256_store_pd(&out[4][0][0],_mm256_permute2f128_pd(shufla,shuflc,0x20));
    // f0,f1,f2,f3
    _mm256_store_pd(&out[5][0][0],_mm256_permute2f128_pd(shuflb,shufld,0x20));
    // g0,g1,g2,g3
    _mm256_store_pd(&out[6][0][0],_mm256_permute2f128_pd(shufla,shuflc,0x31));
    // h0,h1,h2,h3 -- not required

    // 2
    // a0,b0,c0,d0
    v0 = _mm256_load_pd(&data[pos[0]].var[1][0]);
    // e0,f0,g0,h0
    va = _mm256_load_pd(&data[pos[0]].var[1][4]);
    // a1,b1,c1,d1
    v1 = _mm256_load_pd(&data[pos[1]].var[1][0]);
    // e1,f1,g1,h1
    vb = _mm256_load_pd(&data[pos[1]].var[1][4]);
    // a2,b2,c2,d2
    v2 = _mm256_load_pd(&data[pos[2]].var[1][0]);
    // e2,f2,g2,h2
    vc = _mm256_load_pd(&data[pos[2]].var[1][4]);
    // a3,b3,c3,d3
    v3 = _mm256_load_pd(&data[pos[3]].var[1][0]);
    // e3,f3,g3,h3
    vd = _mm256_load_pd(&data[pos[3]].var[1][4]);

    // 64-bit wide permutations
    // a0,a1,c0,c1
    shufl0 = _mm256_shuffle_pd(v0,v1,0x0);
    // e0,e1,g0,g1
    shufla = _mm256_shuffle_pd(va,vb,0x0);
    // b0,b1,d0,d1
    shufl1 = _mm256_shuffle_pd(v0,v1,0xF);
    // f0,f1,h0,h1
    shuflb = _mm256_shuffle_pd(va,vb,0xF);
    // a2,a3,c2,c3
    shufl2 = _mm256_shuffle_pd(v2,v3,0x0);
    // e2,e3,g2,g3
    shuflc = _mm256_shuffle_pd(vc,vd,0x0);
    // b2,b3,d2,d3
    shufl3 = _mm256_shuffle_pd(v2,v3,0xF);
    // f2,f3,h2,h3
    shufld = _mm256_shuffle_pd(vc,vd,0xF);

    // 128-bit wide permutations and store
    // a0,a1,a2,3
    _mm256_store_pd(&out[0][1][0],_mm256_permute2f128_pd(shufl0,shufl2,0x20));
    // b0,b1,b2,b3
    _mm256_store_pd(&out[1][1][0],_mm256_permute2f128_pd(shufl1,shufl3,0x20));
    // c0,c1,c2,c3
    _mm256_store_pd(&out[2][1][0],_mm256_permute2f128_pd(shufl0,shufl2,0x31));
    // d0,d1,d2,d3
    _mm256_store_pd(&out[3][1][0],_mm256_permute2f128_pd(shufl1,shufl3,0x31));
    // e0,e1,e2,e3
    _mm256_store_pd(&out[4][1][0],_mm256_permute2f128_pd(shufla,shuflc,0x20));
    // f0,f1,f2,f3
    _mm256_store_pd(&out[5][1][0],_mm256_permute2f128_pd(shuflb,shufld,0x20));
    // g0,g1,g2,g3
    _mm256_store_pd(&out[6][1][0],_mm256_permute2f128_pd(shufla,shuflc,0x31));
    // h0,h1,h2,h3 -- not required

    // 3
    // a0,b0,c0,d0
    v0 = _mm256_load_pd(&data[pos[0]].var[2][0]);
    // e0,f0,g0,h0
    va = _mm256_load_pd(&data[pos[0]].var[2][4]);
    // a1,b1,c1,d1
    v1 = _mm256_load_pd(&data[pos[1]].var[2][0]);
    // e1,f1,g1,h1
    vb = _mm256_load_pd(&data[pos[1]].var[2][4]);
    // a2,b2,c2,d2
    v2 = _mm256_load_pd(&data[pos[2]].var[2][0]);
    // e2,f2,g2,h2
    vc = _mm256_load_pd(&data[pos[2]].var[2][4]);
    // a3,b3,c3,d3
    v3 = _mm256_load_pd(&data[pos[3]].var[2][0]);
    // e3,f3,g3,h3
    vd = _mm256_load_pd(&data[pos[3]].var[2][4]);

    // 64-bit wide permutations
    // a0,a1,c0,c1
    shufl0 = _mm256_shuffle_pd(v0,v1,0x0);
    // e0,e1,g0,g1
    shufla = _mm256_shuffle_pd(va,vb,0x0);
    // b0,b1,d0,d1
    shufl1 = _mm256_shuffle_pd(v0,v1,0xF);
    // f0,f1,h0,h1
    shuflb = _mm256_shuffle_pd(va,vb,0xF);
    // a2,a3,c2,c3
    shufl2 = _mm256_shuffle_pd(v2,v3,0x0);
    // e2,e3,g2,g3
    shuflc = _mm256_shuffle_pd(vc,vd,0x0);
    // b2,b3,d2,d3
    shufl3 = _mm256_shuffle_pd(v2,v3,0xF);
    // f2,f3,h2,h3
    shufld = _mm256_shuffle_pd(vc,vd,0xF);

    // 128-bit wide permutations and store
    // a0,a1,a2,3
    _mm256_store_pd(&out[0][2][0],_mm256_permute2f128_pd(shufl0,shufl2,0x20));
    // b0,b1,b2,b3
    _mm256_store_pd(&out[1][2][0],_mm256_permute2f128_pd(shufl1,shufl3,0x20));
    // c0,c1,c2,c3
    _mm256_store_pd(&out[2][2][0],_mm256_permute2f128_pd(shufl0,shufl2,0x31));
    // d0,d1,d2,d3
    _mm256_store_pd(&out[3][2][0],_mm256_permute2f128_pd(shufl1,shufl3,0x31));
    // e0,e1,e2,e3
    _mm256_store_pd(&out[4][2][0],_mm256_permute2f128_pd(shufla,shuflc,0x20));
    // f0,f1,f2,f3
    _mm256_store_pd(&out[5][2][0],_mm256_permute2f128_pd(shuflb,shufld,0x20));
    // g0,g1,g2,g3
    _mm256_store_pd(&out[6][2][0],_mm256_permute2f128_pd(shufla,shuflc,0x31));
    // h0,h1,h2,h3 -- not required
}

template < typename type > inline __attribute__((always_inline)) 
void gather1x9(type *data, int *pos, double out[][VECLEN])
{
    // a0,b0,c0,d0
    __m256d v0 = _mm256_load_pd(&data[pos[0]].var[0]);
    // e0,f0,g0,h0
    __m256d va = _mm256_load_pd(&data[pos[0]].var[4]);
    // a1,b1,c1,d1
    __m256d v1 = _mm256_load_pd(&data[pos[1]].var[0]);
    // e1,f1,g1,h1
    __m256d vb = _mm256_load_pd(&data[pos[1]].var[4]);
    // a2,b2,c2,d2
    __m256d v2 = _mm256_load_pd(&data[pos[2]].var[0]);
    // e2,f2,g2,h2
    __m256d vc = _mm256_load_pd(&data[pos[2]].var[4]);
    // a3,b3,c3,d3
    __m256d v3 = _mm256_load_pd(&data[pos[3]].var[0]);
    // e3,f3,g3,h3
    __m256d vd = _mm256_load_pd(&data[pos[3]].var[4]);

    // 64-bit wide permutations
    // a0,a1,c0,c1
    __m256d shufl0 = _mm256_shuffle_pd(v0,v1,0x0);
    // e0,e1,g0,g1
    __m256d shufla = _mm256_shuffle_pd(va,vb,0x0);
    // b0,b1,d0,d1
    __m256d shufl1 = _mm256_shuffle_pd(v0,v1,0xF);
    // f0,f1,h0,h1
    __m256d shuflb = _mm256_shuffle_pd(va,vb,0xF);
    // a2,a3,c2,c3
    __m256d shufl2 = _mm256_shuffle_pd(v2,v3,0x0);
    // e2,e3,g2,g3
    __m256d shuflc = _mm256_shuffle_pd(vc,vd,0x0);
    // b2,b3,d2,d3
    __m256d shufl3 = _mm256_shuffle_pd(v2,v3,0xF);
    // f2,f3,h2,h3
    __m256d shufld = _mm256_shuffle_pd(vc,vd,0xF);

    // 128-bit wide permutations and store
    // a0,a1,a2,3
    _mm256_store_pd(&out[0][0],_mm256_permute2f128_pd(shufl0,shufl2,0x20));
    // b0,b1,b2,b3
    _mm256_store_pd(&out[1][0],_mm256_permute2f128_pd(shufl1,shufl3,0x20));
    // c0,c1,c2,c3
    _mm256_store_pd(&out[2][0],_mm256_permute2f128_pd(shufl0,shufl2,0x31));
    // d0,d1,d2,d3
    _mm256_store_pd(&out[3][0],_mm256_permute2f128_pd(shufl1,shufl3,0x31));
    // e0,e1,e2,e3
    _mm256_store_pd(&out[4][0],_mm256_permute2f128_pd(shufla,shuflc,0x20));
    // f0,f1,f2,f3
    _mm256_store_pd(&out[5][0],_mm256_permute2f128_pd(shuflb,shufld,0x20));
    // g0,g1,g2,g3
    _mm256_store_pd(&out[6][0],_mm256_permute2f128_pd(shufla,shuflc,0x31));
    // h0,h1,h2,h3
    _mm256_store_pd(&out[7][0],_mm256_permute2f128_pd(shuflb,shufld,0x31));
    // do last values serially for now until we think of a better way
    // perhaps a masked mm_load? might not be worth it
    // i0,i1,i2,i3
    out[8][0] = data[pos[0]].var[8];
    out[8][1] = data[pos[1]].var[8];
    out[8][2] = data[pos[2]].var[8];
    out[8][3] = data[pos[3]].var[8];
}

template < typename type > inline __attribute__((always_inline)) 
void scatter1x7(double data[][VECLEN], int *pos, type *out)
{
    __m256d v0,v1,v2,v3,va,vb,vc,vd;
    __m256d wrk0,wrk1,wrk2,wrk3,wrk4,wrk5,wrk6,wrk7;
    __m256d shufl0,shufl1,shufl2,shufl3;
    __m256d shufla,shuflb,shuflc,shufld;

    // a0,a1,a2,a3
    v0 = _mm256_load_pd(&data[0][0]);
    // b0,b1,b2,b3
    v1 = _mm256_load_pd(&data[1][0]);
    // c0,c1,c2,c3
    v2 = _mm256_load_pd(&data[2][0]);
    // d0,d1,d2,d3
    v3 = _mm256_load_pd(&data[3][0]);
    // e0,e1,e2,e3
    va = _mm256_load_pd(&data[4][0]);
    // f0,f1,f2,f3
    vb = _mm256_load_pd(&data[5][0]);
    // g0,g1,g2,g3
    vc = _mm256_load_pd(&data[6][0]);
    // h0,h1,h2,h3
    vd = _mm256_set1_pd(0.);

    // 128-bit wide permutations
    // a0,a1,c0,c1
    wrk0 = _mm256_permute2f128_pd(v0,v2,0x20);
    // b0,b1,d0,d1
    wrk1 = _mm256_permute2f128_pd(v1,v3,0x20);
    // a2,a3,c2,c3
    wrk2 = _mm256_permute2f128_pd(v0,v2,0x31);
    // b2,b3,d2,d3
    wrk3 = _mm256_permute2f128_pd(v1,v3,0x31);
    // e0,e1,g0,g1
    wrk4 = _mm256_permute2f128_pd(va,vc,0x20);
    // f0,f1,h0,h1
    wrk5 = _mm256_permute2f128_pd(vb,vd,0x20);
    // e2,e3,g2,g3
    wrk6 = _mm256_permute2f128_pd(va,vc,0x31);
    // f2,f3,h2,h3
    wrk7 = _mm256_permute2f128_pd(vb,vd,0x31);

    // 64-bit wide permutations
    // a0,b0,c0,d0
    shufl0 = _mm256_shuffle_pd(wrk0,wrk1,0x0);
    // a1,b1,c1,d1
    shufl1 = _mm256_shuffle_pd(wrk0,wrk1,0xF);
    // a2,b2,c2,d2
    shufl2 = _mm256_shuffle_pd(wrk2,wrk3,0x0);
    // a3,b3,c3,d3
    shufl3 = _mm256_shuffle_pd(wrk2,wrk3,0xF);
    // e0,f0,g0,h0
    shufla = _mm256_shuffle_pd(wrk4,wrk5,0x0);
    // e1,f1,g1,h1
    shuflb = _mm256_shuffle_pd(wrk4,wrk5,0xF);
    // e2,f2,g2,h2
    shuflc = _mm256_shuffle_pd(wrk6,wrk7,0x0);
    // e3,f3,g3,h3
    shufld = _mm256_shuffle_pd(wrk6,wrk7,0xF);

    _mm256_store_pd(&out[pos[0]].var[0], shufl0);
    _mm256_store_pd(&out[pos[0]].var[4], shufla);
    _mm256_store_pd(&out[pos[1]].var[0], shufl1);
    _mm256_store_pd(&out[pos[1]].var[4], shuflb);
    _mm256_store_pd(&out[pos[2]].var[0], shufl2);
    _mm256_store_pd(&out[pos[2]].var[4], shuflc);
    _mm256_store_pd(&out[pos[3]].var[0], shufl3);
    _mm256_store_pd(&out[pos[3]].var[4], shufld);
}

# endif
