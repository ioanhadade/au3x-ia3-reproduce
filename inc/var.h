# ifndef __VAR_H__
# define __VAR_H__
/**
 * This is a very crude example but should do the job.
 * Ideally, a class/container is preferred to encapsulate
 * the underlying data layout and dictate the access to
 * elements etc.
 * We have to make sure that each internal structure is also aligned.
 *
 **/

# include <sizes.h>

typedef struct
{
    double var[4]                 __attribute__((aligned(ALIGN)));
} aos1x4_t;

typedef struct
{
    double var[MAXNPDES]          __attribute__((aligned(ALIGN)));
} aos1x8_t;

typedef struct
{
    double var[3][MAXNPDES]       __attribute__((aligned(ALIGN)));
} aos3x8_t;

typedef struct
{
    double var[9]                 __attribute__((aligned(ALIGN)));
} aos1x9_t;

# endif
