# ifndef __VEC_H__
# define __VEC_H__

# include <immintrin.h>
# include <sizes.h>
# include <var.h>
# include <prefetch.h>

#if defined __AVX512F__
    #include <vec512.h>
#elif defined __AVX__
    #include <vec256.h>
#elif defined __MIC__
    #include <vecimci.h>
#endif

template < typename type > inline __attribute__((always_inline)) void 
vgather(type *data, int nvar, int *index, double out[][VECLEN])
{
# ifdef ENABLE_PF
    prefetchd(data, index);
# endif
    if(nvar==7)
    {
        gather1x7(data, index, out);
    }
    else if(nvar==3)
    {
        gather1x3(data, index, out);
    }
    else if(nvar==9)
    {
        gather1x9(data, index, out);
    }
}

template < typename type > inline __attribute__((always_inline)) void 
vgather(type *data, int ndim, int nvar, int *index, double out[][3][VECLEN])
{
    if(ndim==3)
    {
        if(nvar==7)
        {
# ifdef ENABLE_PF
            prefetchd(data, index, 0);
            prefetchd(data, index, 1);
            prefetchd(data, index, 2);
# endif
            gather3x7(data, index, out);
        }
    }
}

template < typename type > inline __attribute__((always_inline)) void 
vload(type *data, int nvar, int offset, double out[][VECLEN])
{
    if(nvar==7)
    {
# ifdef ENABLE_PF
        prefetchd(data, offset);
# endif
        load1x7(data, offset, out);
    }
}

template < typename type > inline __attribute__((always_inline)) void 
vscatter(double data[][VECLEN], int nvar, int *index, type *out)
{
    if(nvar==7)
    {
        scatter1x7(data, index, out);
    }
}

inline __attribute__((always_inline)) void
vload(double *data[], int nvar, int offset, double out[][VECLEN])
{
#ifdef ENABLE_PF
    prefetchd(data,nvar,offset);
#endif
    load(data,nvar,offset,out);
}

inline __attribute__((always_inline)) void
vload(double *data[], int offset, double out[VECLEN])
{
#ifdef ENABLE_PF
    prefetchd(data,1,offset);
#endif
    load(data,offset,out);
}
# endif
