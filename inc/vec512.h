# ifndef __VEC512_H__
# define __VEC512_H__

// 1111 1111
const __mmask8 mask0xFF = 0xFF;
// all bits are unset
const __mmask8 mask0x0  = 0x0;
// 0011 0011
const __mmask8 mask0x33 = 0x33;
// 1100 1100
const __mmask8 mask0xCC = 0xCC;
// 1111 0000
const __mmask8 mask0xF0 = 0xF0;
// 0000 1111
const __mmask8 mask0xF  = 0xF;
// 0000 0001
const __mmask8 mask0x1  = 0x1;
// 0010 0010
const __mmask8 mask0x22 = 0x22;
// 1000 1000
const __mmask8 mask0x88 = 0x88;
// 0111 1111
const __mmask8 mask0x7F = 0x7F;
// 0000 0111
const __mmask8 mask0x7  = 0x7;
// 0111 1110
const __mmask8 mask0x7E = 0x7E;
// 1111 1110
const __mmask8 mask0xFE = 0xFE;


inline __attribute__((always_inline)) void
load(double *data[], int nvar, int offset, double out[][VECLEN])
{
   int ivar;
   __m512d v;

   for( ivar=0;ivar<nvar;ivar++ )
   {
      v= _mm512_load_pd(&data[ivar][offset]);
      _mm512_store_pd(&out[ivar][0],v);
   }
}


inline __attribute__((always_inline)) void
load(double *data[], int offset, double out[VECLEN])
{
    __m512d v;

    v= _mm512_load_pd(&data[0][offset]);
    _mm512_store_pd(&out[0],v);
}


template < typename type > inline __attribute__((always_inline))
void gather1x7(type *data, int *pos, double out[][VECLEN])
{
    __m512d v[8];

    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0] = _mm512_load_pd(&data[pos[0]].var[0]);
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1] = _mm512_load_pd(&data[pos[1]].var[0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2] = _mm512_load_pd(&data[pos[2]].var[0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3] = _mm512_load_pd(&data[pos[3]].var[0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4] = _mm512_load_pd(&data[pos[4]].var[0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5] = _mm512_load_pd(&data[pos[5]].var[0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6] = _mm512_load_pd(&data[pos[6]].var[0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7] = _mm512_load_pd(&data[pos[7]].var[0]);

    // interleave low part
    // a0,a1,c0,c1,e0,e1,g0,g1
    __m512d a0a1c0c1e0e1g0g1 = _mm512_unpacklo_pd(v[0], v[1]);
    // a2,a3,c2,c3,e2,e3,g2,g3
    __m512d a2a3c2c3e2e3g2g3 = _mm512_unpacklo_pd(v[2], v[3]);
    // a4,a5,c4,c5,e4,e5,g4,g5
    __m512d a4a5c4c5e4e5g4g5 = _mm512_unpacklo_pd(v[4], v[5]);
    // a6,a7,c6,c7,e6,e7,g6,g7
    __m512d a6a7c6c7e6e7g6g7 = _mm512_unpacklo_pd(v[6], v[7]);

    // interleave high part
    // b0,b1,d0,d1,f0,f1,h0,h1
    __m512d b0b1d0d1f0f1h0h1 = _mm512_unpackhi_pd(v[0], v[1]);
    // b2,b3,d2,d3,f2,f3,h2,h3
    __m512d b2b3d2d3f2f3h2h3 = _mm512_unpackhi_pd(v[2], v[3]);
    // b4,b5,d4,d5,f4,f5,h4,h5
    __m512d b4b5d4d5f4f5h4h5 = _mm512_unpackhi_pd(v[4], v[5]);
    // b6,b7,d6,d7,f6,f7,h6,h7
    __m512d b6b7d6d7f6f7h6h7 = _mm512_unpackhi_pd(v[6], v[7]);

    // a0,a1,a2,a3,e0,e1,e2,e3
    __m512d a0a1a2a3e0e1e2e3 = _mm512_mask_permutex_pd(a0a1c0c1e0e1g0g1, mask0xCC, a2a3c2c3e2e3g2g3, 0x44);
    // b0,b1,b2,b3,f0,f1,f2,f3
    __m512d b0b1b2b3f0f1f2f3 = _mm512_mask_permutex_pd(b0b1d0d1f0f1h0h1, mask0xCC, b2b3d2d3f2f3h2h3, 0x44);
    // c0,c1,c2,c3,g0,g1,g2,g3
    __m512d c0c1c2c3g0g1g2g3 = _mm512_mask_permutex_pd(a2a3c2c3e2e3g2g3, mask0x33, a0a1c0c1e0e1g0g1, 0xEE);
    // d0,d1,d2,d3,h0,h1,h2,h3
    __m512d d0d1d2d3h0h1h2h3 = _mm512_mask_permutex_pd(b2b3d2d3f2f3h2h3, mask0x33, b0b1d0d1f0f1h0h1, 0xEE);
    // a4,a5,a6,a7,e4,e5,e6,e7
    __m512d a4a5a6a7e4e5e6e7 = _mm512_mask_permutex_pd(a4a5c4c5e4e5g4g5, mask0xCC, a6a7c6c7e6e7g6g7, 0x44);
    // b4,b5,b6,b7,f4,f5,f6,f7
    __m512d b4b5b6b7f4f5f6f7 = _mm512_mask_permutex_pd(b4b5d4d5f4f5h4h5, mask0xCC, b6b7d6d7f6f7h6h7, 0x44);
    // c4,c5,c6,c7,g4,g5,g6,g7
    __m512d c4c5c6c7g4g5g6g7 = _mm512_mask_permutex_pd(a6a7c6c7e6e7g6g7, mask0x33, a4a5c4c5e4e5g4g5, 0xEE);
    // d4,d5,d6,d7,h4,h5,h6,h7
    __m512d d4d5d6d7h4h5h6h7 = _mm512_mask_permutex_pd(b6b7d6d7f6f7h6h7, mask0x33, b4b5d4d5f4f5h4h5, 0xEE);

    // a0,a1,a2,a3,a4,a5,a6,a7,a8
    __m512d a0a1a2a3a4a5a6a7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0x44);
    // b0,b1,b2,b3,b4,b5,b6,b7
    __m512d b0b1b2b3b4b5b6b7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0x44);
    // c0,c1,c2,c3,c4,c5,c6,c7
    __m512d c0c1c2c3c4c5c6c7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0x44);
    // d0,d1,d2,d3,d4,d5,d6,d7
    __m512d d0d1d2d3d4d5d6d7 = _mm512_shuffle_f64x2(d0d1d2d3h0h1h2h3, d4d5d6d7h4h5h6h7, 0x44);
    // e0,e1,e2,e3,e4,e5,e6,e7
    __m512d e0e1e2e3e4e5e6e7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0xEE);
    // f0,f1,f2,f3,f4,f5,f6,f7
    __m512d f0f1f2f3f4f5f6f7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0xEE);
    // g0,g1,g2,g3,g4,g5,g6,g7
    __m512d g0g1g2g3g4g5g6g7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0xEE);
    // h0,h1,h2,h3,h4,h5,h6,h7 -- not required.

    _mm512_store_pd(&out[0][0], a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0], b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0], c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][0], d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][0], e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][0], f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][0], g0g1g2g3g4g5g6g7);
}


template < typename type > inline __attribute__((always_inline))
void gather1x3(type *data, int *pos, double out[][VECLEN])
{
    // a0,b0,c0,d0,a4,b4,c4,d4
    __m512d a0b0c0d0a4b4c4d4 = _mm512_mask_broadcast_f64x4(_mm512_mask_load_pd(_mm512_undefined_pd(), mask0xF,
                               &data[pos[0]].var[0]), mask0xF0, _mm256_load_pd(&data[pos[4]].var[0]));
    // a1,b3,c1,d1,a5,b5,c5,d5
    __m512d a1b1c1d1a5b5c5d5 = _mm512_mask_broadcast_f64x4(_mm512_mask_load_pd(_mm512_undefined_pd(), mask0xF,
                               &data[pos[1]].var[0]), mask0xF0, _mm256_load_pd(&data[pos[5]].var[0]));
    // a2,b2,c2,d2,a6,b6,c6,d6
    __m512d a2b2c2d2a6b6c6d6 = _mm512_mask_broadcast_f64x4(_mm512_mask_load_pd(_mm512_undefined_pd(), mask0xF,
                               &data[pos[2]].var[0]), mask0xF0, _mm256_load_pd(&data[pos[6]].var[0]));
    // a3,b3,c3,d3,a7,b7,c7,d7
    __m512d a3b3c3d3a7b7c7d7 = _mm512_mask_broadcast_f64x4(_mm512_mask_load_pd(_mm512_undefined_pd(), mask0xF,
                               &data[pos[3]].var[0]), mask0xF0, _mm256_load_pd(&data[pos[7]].var[0]));

    // interleave low and high parts
    // a0,a1,c0,c1,a4,a5,c4,c5
    __m512d a0a1c0c1a4a5c4c5 = _mm512_unpacklo_pd(a0b0c0d0a4b4c4d4, a1b1c1d1a5b5c5d5);
    // b0,b1,d0,d1,b4,b5,d4,d5
    __m512d b0b1d0d1b4b5d4d5 = _mm512_unpackhi_pd(a0b0c0d0a4b4c4d4, a1b1c1d1a5b5c5d5);
    // a2,a3,c2,c3,a6,a7,c6,c7
    __m512d a2a3c2c3a6a7c6c7 = _mm512_unpacklo_pd(a2b2c2d2a6b6c6d6, a3b3c3d3a7b7c7d7);
    // b2,b3,d2,d3,b6,b7,d6,d7
    __m512d b2b3d2d3b6b7d6d7 = _mm512_unpackhi_pd(a2b2c2d2a6b6c6d6, a3b3c3d3a7b7c7d7);

    // a0,a1,a2,a3,a4,a5,a6,a7
    __m512d a0a1a2a3a4a5a6a7 = _mm512_mask_permutex_pd(a0a1c0c1a4a5c4c5, mask0xCC, a2a3c2c3a6a7c6c7, 0x44);
    // b0,b1,b2,b3,b4,b5,b6,b7
    __m512d b0b1b2b3b4b5b6b7 = _mm512_mask_permutex_pd(b0b1d0d1b4b5d4d5, mask0xCC, b2b3d2d3b6b7d6d7, 0x44);
    // c0,c1,c2,c3,c4,c5,c6,c7
    __m512d c0c1c2c3c4c5c6c7 = _mm512_mask_permutex_pd(a2a3c2c3a6a7c6c7, mask0x33, a0a1c0c1a4a5c4c5, 0xEE);

    _mm512_store_pd(&out[0][0], a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0], b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0], c0c1c2c3c4c5c6c7);
}

template < typename type > inline __attribute__((always_inline))
void gather3x7(type *data, int *pos, double out[][3][VECLEN])
{
    __m512d v[8];
    __m512d a0a1c0c1e0e1g0g1, a2a3c2c3e2e3g2g3, a4a5c4c5e4e5g4g5, a6a7c6c7e6e7g6g7;
    __m512d b0b1d0d1f0f1h0h1, b2b3d2d3f2f3h2h3, b4b5d4d5f4f5h4h5, b6b7d6d7f6f7h6h7;
    __m512d a0a1a2a3e0e1e2e3, b0b1b2b3f0f1f2f3, c0c1c2c3g0g1g2g3, d0d1d2d3h0h1h2h3;
    __m512d a4a5a6a7e4e5e6e7, b4b5b6b7f4f5f6f7, c4c5c6c7g4g5g6g7, d4d5d6d7h4h5h6h7;
    __m512d e0e1e2e3a4a5a6a7, f0f1f2f3b4b5b6b7, g0g1g2g3c4c5c6c7, h0h1h2h3d4d5d6d7;
    __m512d a0a1a2a3a4a5a6a7, b0b1b2b3b4b5b6b7, c0c1c2c3c4c5c6c7, d0d1d2d3d4d5d6d7;
    __m512d e0e1e2e3e4e5e6e7, f0f1f2f3f4f5f6f7, g0g1g2g3g4g5g6g7;

    // 1
    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0] = _mm512_load_pd(&data[pos[0]].var[0][0]);
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1] = _mm512_load_pd(&data[pos[1]].var[0][0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2] = _mm512_load_pd(&data[pos[2]].var[0][0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3] = _mm512_load_pd(&data[pos[3]].var[0][0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4] = _mm512_load_pd(&data[pos[4]].var[0][0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5] = _mm512_load_pd(&data[pos[5]].var[0][0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6] = _mm512_load_pd(&data[pos[6]].var[0][0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7] = _mm512_load_pd(&data[pos[7]].var[0][0]);

    // interleave low part
    // a0,a1,c0,c1,e0,e1,g0,g1
    a0a1c0c1e0e1g0g1 = _mm512_unpacklo_pd(v[0], v[1]);
    // a2,a3,c2,c3,e2,e3,g2,g3
    a2a3c2c3e2e3g2g3 = _mm512_unpacklo_pd(v[2], v[3]);
    // a4,a5,c4,c5,e4,e5,g4,g5
    a4a5c4c5e4e5g4g5 = _mm512_unpacklo_pd(v[4], v[5]);
    // a6,a7,c6,c7,e6,e7,g6,g7
    a6a7c6c7e6e7g6g7 = _mm512_unpacklo_pd(v[6], v[7]);

    // interleave high part
    // b0,b1,d0,d1,f0,f1,h0,h1
    b0b1d0d1f0f1h0h1 = _mm512_unpackhi_pd(v[0], v[1]);
    // b2,b3,d2,d3,f2,f3,h2,h3
    b2b3d2d3f2f3h2h3 = _mm512_unpackhi_pd(v[2], v[3]);
    // b4,b5,d4,d5,f4,f5,h4,h5
    b4b5d4d5f4f5h4h5 = _mm512_unpackhi_pd(v[4], v[5]);
    // b6,b7,d6,d7,f6,f7,h6,h7
    b6b7d6d7f6f7h6h7 = _mm512_unpackhi_pd(v[6], v[7]);

    // a0,a1,a2,a3,e0,e1,e2,e3
    a0a1a2a3e0e1e2e3 = _mm512_mask_permutex_pd(a0a1c0c1e0e1g0g1, mask0xCC, a2a3c2c3e2e3g2g3, 0x44);
    // b0,b1,b2,b3,f0,f1,f2,f3
    b0b1b2b3f0f1f2f3 = _mm512_mask_permutex_pd(b0b1d0d1f0f1h0h1, mask0xCC, b2b3d2d3f2f3h2h3, 0x44);
    // c0,c1,c2,c3,g0,g1,g2,g3
    c0c1c2c3g0g1g2g3 = _mm512_mask_permutex_pd(a2a3c2c3e2e3g2g3, mask0x33, a0a1c0c1e0e1g0g1, 0xEE);
    // d0,d1,d2,d3,h0,h1,h2,h3
    d0d1d2d3h0h1h2h3 = _mm512_mask_permutex_pd(b2b3d2d3f2f3h2h3, mask0x33, b0b1d0d1f0f1h0h1, 0xEE);
    // a4,a5,a6,a7,e4,e5,e6,e7
    a4a5a6a7e4e5e6e7 = _mm512_mask_permutex_pd(a4a5c4c5e4e5g4g5, mask0xCC, a6a7c6c7e6e7g6g7, 0x44);
    // b4,b5,b6,b7,f4,f5,f6,f7
    b4b5b6b7f4f5f6f7 = _mm512_mask_permutex_pd(b4b5d4d5f4f5h4h5, mask0xCC, b6b7d6d7f6f7h6h7, 0x44);
    // c4,c5,c6,c7,g4,g5,g6,g7
    c4c5c6c7g4g5g6g7 = _mm512_mask_permutex_pd(a6a7c6c7e6e7g6g7, mask0x33, a4a5c4c5e4e5g4g5, 0xEE);
    // d4,d5,d6,d7,h4,h5,h6,h7
    d4d5d6d7h4h5h6h7 = _mm512_mask_permutex_pd(b6b7d6d7f6f7h6h7, mask0x33, b4b5d4d5f4f5h4h5, 0xEE);

    a0a1a2a3a4a5a6a7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0x44);
    b0b1b2b3b4b5b6b7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0x44);
    c0c1c2c3c4c5c6c7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0x44);
    d0d1d2d3d4d5d6d7 = _mm512_shuffle_f64x2(d0d1d2d3h0h1h2h3, d4d5d6d7h4h5h6h7, 0x44);
    e0e1e2e3e4e5e6e7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0xEE);
    f0f1f2f3f4f5f6f7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0xEE);
    g0g1g2g3g4g5g6g7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0xEE);

    _mm512_store_pd(&out[0][0][0], a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0][0], b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0][0], c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][0][0], d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][0][0], e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][0][0], f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][0][0], g0g1g2g3g4g5g6g7);

    // 1
    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0] = _mm512_load_pd(&data[pos[0]].var[1][0]);
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1] = _mm512_load_pd(&data[pos[1]].var[1][0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2] = _mm512_load_pd(&data[pos[2]].var[1][0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3] = _mm512_load_pd(&data[pos[3]].var[1][0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4] = _mm512_load_pd(&data[pos[4]].var[1][0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5] = _mm512_load_pd(&data[pos[5]].var[1][0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6] = _mm512_load_pd(&data[pos[6]].var[1][0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7] = _mm512_load_pd(&data[pos[7]].var[1][0]);

    // interleave low part
    // a0,a1,c0,c1,e0,e1,g0,g1
    a0a1c0c1e0e1g0g1 = _mm512_unpacklo_pd(v[0], v[1]);
    // a2,a3,c2,c3,e2,e3,g2,g3
    a2a3c2c3e2e3g2g3 = _mm512_unpacklo_pd(v[2], v[3]);
    // a4,a5,c4,c5,e4,e5,g4,g5
    a4a5c4c5e4e5g4g5 = _mm512_unpacklo_pd(v[4], v[5]);
    // a6,a7,c6,c7,e6,e7,g6,g7
    a6a7c6c7e6e7g6g7 = _mm512_unpacklo_pd(v[6], v[7]);

    // interleave high part
    // b0,b1,d0,d1,f0,f1,h0,h1
    b0b1d0d1f0f1h0h1 = _mm512_unpackhi_pd(v[0], v[1]);
    // b2,b3,d2,d3,f2,f3,h2,h3
    b2b3d2d3f2f3h2h3 = _mm512_unpackhi_pd(v[2], v[3]);
    // b4,b5,d4,d5,f4,f5,h4,h5
    b4b5d4d5f4f5h4h5 = _mm512_unpackhi_pd(v[4], v[5]);
    // b6,b7,d6,d7,f6,f7,h6,h7
    b6b7d6d7f6f7h6h7 = _mm512_unpackhi_pd(v[6], v[7]);

    // a0,a1,a2,a3,e0,e1,e2,e3
    a0a1a2a3e0e1e2e3 = _mm512_mask_permutex_pd(a0a1c0c1e0e1g0g1, mask0xCC, a2a3c2c3e2e3g2g3, 0x44);
    // b0,b1,b2,b3,f0,f1,f2,f3
    b0b1b2b3f0f1f2f3 = _mm512_mask_permutex_pd(b0b1d0d1f0f1h0h1, mask0xCC, b2b3d2d3f2f3h2h3, 0x44);
    // c0,c1,c2,c3,g0,g1,g2,g3
    c0c1c2c3g0g1g2g3 = _mm512_mask_permutex_pd(a2a3c2c3e2e3g2g3, mask0x33, a0a1c0c1e0e1g0g1, 0xEE);
    // d0,d1,d2,d3,h0,h1,h2,h3
    d0d1d2d3h0h1h2h3 = _mm512_mask_permutex_pd(b2b3d2d3f2f3h2h3, mask0x33, b0b1d0d1f0f1h0h1, 0xEE);
    // a4,a5,a6,a7,e4,e5,e6,e7
    a4a5a6a7e4e5e6e7 = _mm512_mask_permutex_pd(a4a5c4c5e4e5g4g5, mask0xCC, a6a7c6c7e6e7g6g7, 0x44);
    // b4,b5,b6,b7,f4,f5,f6,f7
    b4b5b6b7f4f5f6f7 = _mm512_mask_permutex_pd(b4b5d4d5f4f5h4h5, mask0xCC, b6b7d6d7f6f7h6h7, 0x44);
    // c4,c5,c6,c7,g4,g5,g6,g7
    c4c5c6c7g4g5g6g7 = _mm512_mask_permutex_pd(a6a7c6c7e6e7g6g7, mask0x33, a4a5c4c5e4e5g4g5, 0xEE);
    // d4,d5,d6,d7,h4,h5,h6,h7
    d4d5d6d7h4h5h6h7 = _mm512_mask_permutex_pd(b6b7d6d7f6f7h6h7, mask0x33, b4b5d4d5f4f5h4h5, 0xEE);

    a0a1a2a3a4a5a6a7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0x44);
    b0b1b2b3b4b5b6b7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0x44);
    c0c1c2c3c4c5c6c7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0x44);
    d0d1d2d3d4d5d6d7 = _mm512_shuffle_f64x2(d0d1d2d3h0h1h2h3, d4d5d6d7h4h5h6h7, 0x44);
    e0e1e2e3e4e5e6e7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0xEE);
    f0f1f2f3f4f5f6f7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0xEE);
    g0g1g2g3g4g5g6g7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0xEE);

    _mm512_store_pd(&out[0][1][0], a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][1][0], b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][1][0], c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][1][0], d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][1][0], e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][1][0], f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][1][0], g0g1g2g3g4g5g6g7);

    // 2
    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0] = _mm512_load_pd(&data[pos[0]].var[2][0]);
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1] = _mm512_load_pd(&data[pos[1]].var[2][0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2] = _mm512_load_pd(&data[pos[2]].var[2][0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3] = _mm512_load_pd(&data[pos[3]].var[2][0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4] = _mm512_load_pd(&data[pos[4]].var[2][0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5] = _mm512_load_pd(&data[pos[5]].var[2][0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6] = _mm512_load_pd(&data[pos[6]].var[2][0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7] = _mm512_load_pd(&data[pos[7]].var[2][0]);

    // interleave low part
    // a0,a1,c0,c1,e0,e1,g0,g1
    a0a1c0c1e0e1g0g1 = _mm512_unpacklo_pd(v[0], v[1]);
    // a2,a3,c2,c3,e2,e3,g2,g3
    a2a3c2c3e2e3g2g3 = _mm512_unpacklo_pd(v[2], v[3]);
    // a4,a5,c4,c5,e4,e5,g4,g5
    a4a5c4c5e4e5g4g5 = _mm512_unpacklo_pd(v[4], v[5]);
    // a6,a7,c6,c7,e6,e7,g6,g7
    a6a7c6c7e6e7g6g7 = _mm512_unpacklo_pd(v[6], v[7]);

    // interleave high part
    // b0,b1,d0,d1,f0,f1,h0,h1
    b0b1d0d1f0f1h0h1 = _mm512_unpackhi_pd(v[0], v[1]);
    // b2,b3,d2,d3,f2,f3,h2,h3
    b2b3d2d3f2f3h2h3 = _mm512_unpackhi_pd(v[2], v[3]);
    // b4,b5,d4,d5,f4,f5,h4,h5
    b4b5d4d5f4f5h4h5 = _mm512_unpackhi_pd(v[4], v[5]);
    // b6,b7,d6,d7,f6,f7,h6,h7
    b6b7d6d7f6f7h6h7 = _mm512_unpackhi_pd(v[6], v[7]);

    // a0,a1,a2,a3,e0,e1,e2,e3
    a0a1a2a3e0e1e2e3 = _mm512_mask_permutex_pd(a0a1c0c1e0e1g0g1, mask0xCC, a2a3c2c3e2e3g2g3, 0x44);
    // b0,b1,b2,b3,f0,f1,f2,f3
    b0b1b2b3f0f1f2f3 = _mm512_mask_permutex_pd(b0b1d0d1f0f1h0h1, mask0xCC, b2b3d2d3f2f3h2h3, 0x44);
    // c0,c1,c2,c3,g0,g1,g2,g3
    c0c1c2c3g0g1g2g3 = _mm512_mask_permutex_pd(a2a3c2c3e2e3g2g3, mask0x33, a0a1c0c1e0e1g0g1, 0xEE);
    // d0,d1,d2,d3,h0,h1,h2,h3
    d0d1d2d3h0h1h2h3 = _mm512_mask_permutex_pd(b2b3d2d3f2f3h2h3, mask0x33, b0b1d0d1f0f1h0h1, 0xEE);
    // a4,a5,a6,a7,e4,e5,e6,e7
    a4a5a6a7e4e5e6e7 = _mm512_mask_permutex_pd(a4a5c4c5e4e5g4g5, mask0xCC, a6a7c6c7e6e7g6g7, 0x44);
    // b4,b5,b6,b7,f4,f5,f6,f7
    b4b5b6b7f4f5f6f7 = _mm512_mask_permutex_pd(b4b5d4d5f4f5h4h5, mask0xCC, b6b7d6d7f6f7h6h7, 0x44);
    // c4,c5,c6,c7,g4,g5,g6,g7
    c4c5c6c7g4g5g6g7 = _mm512_mask_permutex_pd(a6a7c6c7e6e7g6g7, mask0x33, a4a5c4c5e4e5g4g5, 0xEE);
    // d4,d5,d6,d7,h4,h5,h6,h7
    d4d5d6d7h4h5h6h7 = _mm512_mask_permutex_pd(b6b7d6d7f6f7h6h7, mask0x33, b4b5d4d5f4f5h4h5, 0xEE);

    a0a1a2a3a4a5a6a7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0x44);
    b0b1b2b3b4b5b6b7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0x44);
    c0c1c2c3c4c5c6c7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0x44);
    d0d1d2d3d4d5d6d7 = _mm512_shuffle_f64x2(d0d1d2d3h0h1h2h3, d4d5d6d7h4h5h6h7, 0x44);
    e0e1e2e3e4e5e6e7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0xEE);
    f0f1f2f3f4f5f6f7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0xEE);
    g0g1g2g3g4g5g6g7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0xEE);

    _mm512_store_pd(&out[0][2][0], a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][2][0], b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][2][0], c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][2][0], d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][2][0], e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][2][0], f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][2][0], g0g1g2g3g4g5g6g7);
}

template < typename type > inline __attribute__((always_inline))
void gather1x9(type *data, int *pos, double out[][VECLEN])
{
    __m512d v[8];

    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0] = _mm512_load_pd(&data[pos[0]].var[0]);
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1] = _mm512_load_pd(&data[pos[1]].var[0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2] = _mm512_load_pd(&data[pos[2]].var[0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3] = _mm512_load_pd(&data[pos[3]].var[0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4] = _mm512_load_pd(&data[pos[4]].var[0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5] = _mm512_load_pd(&data[pos[5]].var[0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6] = _mm512_load_pd(&data[pos[6]].var[0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7] = _mm512_load_pd(&data[pos[7]].var[0]);

    // load remaining i vector
    __m512d i0i0i0i0i4i4i4i4 = _mm512_mask_broadcast_f64x4(_mm512_set1_pd(data[pos[0]].var[8]), mask0xF0,
                               _mm256_set1_pd(data[pos[4]].var[8]));
    __m512d i1i1i1i1i5i5i5i5 = _mm512_mask_broadcast_f64x4(_mm512_set1_pd(data[pos[1]].var[8]), mask0xF0,
                               _mm256_set1_pd(data[pos[5]].var[8]));
    __m512d i2i2i2i2i6i6i6i6 = _mm512_mask_broadcast_f64x4(_mm512_set1_pd(data[pos[2]].var[8]), mask0xF0,
                               _mm256_set1_pd(data[pos[6]].var[8]));
    __m512d i3i3i3i3i7i7i7i7 = _mm512_mask_broadcast_f64x4(_mm512_set1_pd(data[pos[3]].var[8]), mask0xF0,
                               _mm256_set1_pd(data[pos[7]].var[8]));
    // permute the last value here as lots of blends can be scheduled
    // i0,i1,i0,i0,i4,i5,i4,i4
    __m512d i0i1i0i0i4i5i4i4 = _mm512_mask_blend_pd(mask0x22, i0i0i0i0i4i4i4i4, i1i1i1i1i5i5i5i5);
    // i2,i2,i2,i3,i2,i2,i6,i7
    __m512d i2i2i2i3i2i2i6i7 = _mm512_mask_blend_pd(mask0x88, i2i2i2i2i6i6i6i6, i3i3i3i3i7i7i7i7);
    // i0,i1,i2,i3,i4,i5,i6,i7
    __m512d i0i1i2i3i4i5i6i7 = _mm512_mask_blend_pd(mask0xCC, i0i1i0i0i4i5i4i4, i2i2i2i3i2i2i6i7);

    // interleave low part
    // a0,a1,c0,c1,e0,e1,g0,g1
    __m512d a0a1c0c1e0e1g0g1 = _mm512_unpacklo_pd(v[0], v[1]);
    // a2,a3,c2,c3,e2,e3,g2,g3
    __m512d a2a3c2c3e2e3g2g3 = _mm512_unpacklo_pd(v[2], v[3]);
    // a4,a5,c4,c5,e4,e5,g4,g5
    __m512d a4a5c4c5e4e5g4g5 = _mm512_unpacklo_pd(v[4], v[5]);
    // a6,a7,c6,c7,e6,e7,g6,g7
    __m512d a6a7c6c7e6e7g6g7 = _mm512_unpacklo_pd(v[6], v[7]);

    // interleave high part
    // b0,b1,d0,d1,f0,f1,h0,h1
    __m512d b0b1d0d1f0f1h0h1 = _mm512_unpackhi_pd(v[0], v[1]);
    // b2,b3,d2,d3,f2,f3,h2,h3
    __m512d b2b3d2d3f2f3h2h3 = _mm512_unpackhi_pd(v[2], v[3]);
    // b4,b5,d4,d5,f4,f5,h4,h5
    __m512d b4b5d4d5f4f5h4h5 = _mm512_unpackhi_pd(v[4], v[5]);
    // b6,b7,d6,d7,f6,f7,h6,h7
    __m512d b6b7d6d7f6f7h6h7 = _mm512_unpackhi_pd(v[6], v[7]);

    // a0,a1,a2,a3,e0,e1,e2,e3
    __m512d a0a1a2a3e0e1e2e3 = _mm512_mask_permutex_pd(a0a1c0c1e0e1g0g1, mask0xCC, a2a3c2c3e2e3g2g3, 0x44);
    // b0,b1,b2,b3,f0,f1,f2,f3
    __m512d b0b1b2b3f0f1f2f3 = _mm512_mask_permutex_pd(b0b1d0d1f0f1h0h1, mask0xCC, b2b3d2d3f2f3h2h3, 0x44);
    // c0,c1,c2,c3,g0,g1,g2,g3
    __m512d c0c1c2c3g0g1g2g3 = _mm512_mask_permutex_pd(a2a3c2c3e2e3g2g3, mask0x33, a0a1c0c1e0e1g0g1, 0xEE);
    // d0,d1,d2,d3,h0,h1,h2,h3
    __m512d d0d1d2d3h0h1h2h3 = _mm512_mask_permutex_pd(b2b3d2d3f2f3h2h3, mask0x33, b0b1d0d1f0f1h0h1, 0xEE);
    // a4,a5,a6,a7,e4,e5,e6,e7
    __m512d a4a5a6a7e4e5e6e7 = _mm512_mask_permutex_pd(a4a5c4c5e4e5g4g5, mask0xCC, a6a7c6c7e6e7g6g7, 0x44);
    // b4,b5,b6,b7,f4,f5,f6,f7
    __m512d b4b5b6b7f4f5f6f7 = _mm512_mask_permutex_pd(b4b5d4d5f4f5h4h5, mask0xCC, b6b7d6d7f6f7h6h7, 0x44);
    // c4,c5,c6,c7,g4,g5,g6,g7
    __m512d c4c5c6c7g4g5g6g7 = _mm512_mask_permutex_pd(a6a7c6c7e6e7g6g7, mask0x33, a4a5c4c5e4e5g4g5, 0xEE);
    // d4,d5,d6,d7,h4,h5,h6,h7
    __m512d d4d5d6d7h4h5h6h7 = _mm512_mask_permutex_pd(b6b7d6d7f6f7h6h7, mask0x33, b4b5d4d5f4f5h4h5, 0xEE);

    // a0,a1,a2,a3,a4,a5,a6,a7,a8
    __m512d a0a1a2a3a4a5a6a7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0x44);
    // b0,b1,b2,b3,b4,b5,b6,b7
    __m512d b0b1b2b3b4b5b6b7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0x44);
    // c0,c1,c2,c3,c4,c5,c6,c7
    __m512d c0c1c2c3c4c5c6c7 = _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0x44);
    // d0,d1,d2,d3,d4,d5,d6,d7
    __m512d d0d1d2d3d4d5d6d7 = _mm512_shuffle_f64x2(d0d1d2d3h0h1h2h3, d4d5d6d7h4h5h6h7, 0x44);
    // e0,e1,e2,e3,e4,e5,e6,e7
    __m512d e0e1e2e3e4e5e6e7 = _mm512_shuffle_f64x2(a0a1a2a3e0e1e2e3, a4a5a6a7e4e5e6e7, 0xEE);
    // f0,f1,f2,f3,f4,f5,f6,f7
    __m512d f0f1f2f3f4f5f6f7 = _mm512_shuffle_f64x2(b0b1b2b3f0f1f2f3, b4b5b6b7f4f5f6f7, 0xEE);
    // g0,g1,g2,g3,g4,g5,g6,g7
    __m512d g0g1g2g3g4g5g6g7 =  _mm512_shuffle_f64x2(c0c1c2c3g0g1g2g3, c4c5c6c7g4g5g6g7, 0xEE);
    // h0,h1,h2,h3,h4,h5,h6,h7
    __m512d h0h1h2h3h4h5h6h7 = _mm512_shuffle_f64x2(d0d1d2d3h0h1h2h3, d4d5d6d7h4h5h6h7, 0xEE);

    _mm512_store_pd(&out[0][0], a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0], b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0], c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][0], d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][0], e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][0], f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][0], g0g1g2g3g4g5g6g7);
    _mm512_store_pd(&out[7][0], h0h1h2h3h4h5h6h7);
    _mm512_store_pd(&out[8][0], i0i1i2i3i4i5i6i7);
}

template < typename type > inline __attribute__((always_inline)) void
scatter1x7(double data[][VECLEN], int *pos, type *out)
{
    // a0,a1,a2,a3,a4,a5,a6,a7
    __m512d a0a1a2a3a4a5a6a7 = _mm512_load_pd(&data[0][0]);
    // b0,b1,b2,b3,b4,b5,b6,b7
    __m512d b0b1b2b3b4b5b6b7 = _mm512_load_pd(&data[1][0]);
    // c0,c1,c2,c3,c4,c5,c6,c7
    __m512d c0c1c2c3c4c5c6c7 = _mm512_load_pd(&data[2][0]);
    // d0,d1,d2,d3,d4,d5,d6,d7
    __m512d d0d1d2d3d4d5d6d7 = _mm512_load_pd(&data[3][0]);
    // e0,e1,e2,e3,e4,e5,e6,e7
    __m512d e0e1e2e3e4e5e6e7 = _mm512_load_pd(&data[4][0]);
    // f0,f1,f2,f3,f4,f5,f6,f7
    __m512d f0f1f2f3f4f5f6f7 = _mm512_load_pd(&data[5][0]);
    // g0,g1,g2,g3,g4,g5,g6,g7
    __m512d g0g1g2g3g4g5g6g7 = _mm512_load_pd(&data[6][0]);
    // h0,h1,h2,h3,h4,h5,h6,h7 -- no need to load this
    __m512d h0h1h2h3h4h5h6h7 = _mm512_set1_pd(0.);

    // a4,a5,a6,a7,e0,e1,e2,e3
    __m512d a4a5a6a7e0e1e2e3 = _mm512_castsi512_pd(_mm512_alignr_epi64(_mm512_castpd_si512(e0e1e2e3e4e5e6e7),
                               _mm512_castpd_si512(a0a1a2a3a4a5a6a7), 4));
    // a0,a1,a2,a3,e0,e1,e2,e3
    __m512d a0a1a2a3e0e1e2e3 = _mm512_mask_blend_pd(mask0xF0, a0a1a2a3a4a5a6a7, a4a5a6a7e0e1e2e3);
    // a4,a5,a6,a7,e4,e5,e6,e7
    __m512d a4a5a6a7e4e5e6e7 = _mm512_mask_blend_pd(mask0xF0, a4a5a6a7e0e1e2e3, e0e1e2e3e4e5e6e7);
    // b4,b5,b6,b7,f0,f1,f2,f3
    __m512d b4b5b6b7f0f1f2f3 = _mm512_castsi512_pd(_mm512_alignr_epi64(_mm512_castpd_si512(f0f1f2f3f4f5f6f7),
                               _mm512_castpd_si512(b0b1b2b3b4b5b6b7), 4));
    // b0,b1,b2,b3,f0,f1,f2,f3
    __m512d b0b1b2b3f0f1f2f3 = _mm512_mask_blend_pd(mask0xF0, b0b1b2b3b4b5b6b7, b4b5b6b7f0f1f2f3);
    // b4,b5,b6,b7,f4,f5,f6,f7
    __m512d b4b5b6b7f4f5f6f7 = _mm512_mask_blend_pd(mask0xF0, b4b5b6b7f0f1f2f3, f0f1f2f3f4f5f6f7);
    // c4,c5,c6,c7,g0,g1,g2,g3
    __m512d c4c5c6c7g0g1g2g3 = _mm512_castsi512_pd(_mm512_alignr_epi64(_mm512_castpd_si512(g0g1g2g3g4g5g6g7),
                               _mm512_castpd_si512(c0c1c2c3c4c5c6c7), 4));
    // c0,c1,c2,c3,g0,g1,g2,g3
    __m512d c0c1c2c3g0g1g2g3 = _mm512_mask_blend_pd(mask0xF0, c0c1c2c3c4c5c6c7, c4c5c6c7g0g1g2g3);
    // c4,c5,c6,c7,g4,g5,g6,g7
    __m512d c4c5c6c7g4g5g6g7 = _mm512_mask_blend_pd(mask0xF0, c4c5c6c7g0g1g2g3, g0g1g2g3g4g5g6g7);
    // d4,d5,d6,d7,h0,h1,h2,h3
    __m512d d4d5d6d7h0h1h2h3 = _mm512_castsi512_pd(_mm512_alignr_epi64(_mm512_castpd_si512(h0h1h2h3h4h5h6h7),
                               _mm512_castpd_si512(d0d1d2d3d4d5d6d7), 4));

    // d0,d1,d2,d3,h0,h1,h2,h3
    __m512d d0d1d2d3h0h1h2h3 = _mm512_mask_blend_pd(mask0xF0, d0d1d2d3d4d5d6d7, d4d5d6d7h0h1h2h3);
    // d4,d5,d6,d7,h4,h5,h6,h7
    __m512d d4d5d6d7h4h5h6h7 = _mm512_mask_blend_pd(mask0xF0, d4d5d6d7h0h1h2h3, h0h1h2h3h4h5h6h7);
    // a0,a1,c0,c1,e0,e1,g0,g1
    __m512d a0a1c0c1e0e1g0g1 = _mm512_mask_permutex_pd(a0a1a2a3e0e1e2e3, mask0xCC, c0c1c2c3g0g1g2g3, 0x44);
    // a2,a3,c2,c3,e2,e3,g2,g3
    __m512d a2a3c2c3e2e3g2g3 = _mm512_mask_permutex_pd(c0c1c2c3g0g1g2g3, mask0x33, a0a1a2a3e0e1e2e3, 0xEE);
    // a4,a5,c4,c5,e4,e5,g4,g5
    __m512d a4a5c4c5e4e5g4g5 = _mm512_mask_permutex_pd(a4a5a6a7e4e5e6e7, mask0xCC, c4c5c6c7g4g5g6g7, 0x44);
    // a6,a7,c6,c7,e6,e7,g6,g7
    __m512d a6a7c6c7e6e7g6g7 = _mm512_mask_permutex_pd(c4c5c6c7g4g5g6g7, mask0x33, a4a5a6a7e4e5e6e7, 0xEE);
    // b0,b1,d0,d1,f0,f1,h0,h1
    __m512d b0b1d0d1f0f1h0h1 = _mm512_mask_permutex_pd(b0b1b2b3f0f1f2f3, mask0xCC, d0d1d2d3h0h1h2h3, 0x44);
    // b2,b3,d2,d3,f2,f3,h2,h3
    __m512d b2b3d2d3f2f3h2h3 = _mm512_mask_permutex_pd(d0d1d2d3h0h1h2h3, mask0x33, b0b1b2b3f0f1f2f3, 0xEE);
    // b4,b5,d4,d5,f4,f5,h4,h5
    __m512d b4b5d4d5f4f5h4h5 = _mm512_mask_permutex_pd(b4b5b6b7f4f5f6f7, mask0xCC, d4d5d6d7h4h5h6h7, 0x44);
    // b6,b7,d6,d7,f6,f7,h6,h7
    __m512d b6b7d6d7f6f7h6h7 = _mm512_mask_permutex_pd(d4d5d6d7h4h5h6h7, mask0x33, b4b5b6b7f4f5f6f7, 0xEE);

    // a0,b0,c0,d0,e0,f0,g0,h0
    __m512d a0b0c0d0e0f0g0h0 = _mm512_unpacklo_pd(a0a1c0c1e0e1g0g1, b0b1d0d1f0f1h0h1);
    // a1,b1,c1,d1,e1,f1,g1,h1
    __m512d a1b1c1d1e1f1g1h1 = _mm512_unpackhi_pd(a0a1c0c1e0e1g0g1, b0b1d0d1f0f1h0h1);
    // a2,b2,c2,d2,e2,f2,g2,h2
    __m512d a2b2c2d2e2f2g2h2 = _mm512_unpacklo_pd(a2a3c2c3e2e3g2g3, b2b3d2d3f2f3h2h3);
    // a3,b3,c3,d3,e3,f3,g3,h3
    __m512d a3b3c3d3e3f3g3h3 = _mm512_unpackhi_pd(a2a3c2c3e2e3g2g3, b2b3d2d3f2f3h2h3);
    // a4,b4,c4,d4,e4,f4,g4,h4
    __m512d a4b4c4d4e4f4g4h4 = _mm512_unpacklo_pd(a4a5c4c5e4e5g4g5, b4b5d4d5f4f5h4h5);
    // a5,b5,c5,d5,e5,f5,g5,h5
    __m512d a5b5c5d5e5f5g5h5 = _mm512_unpackhi_pd(a4a5c4c5e4e5g4g5, b4b5d4d5f4f5h4h5);
    // a6,b6,c6,d6,e6,f6,g6,h6
    __m512d a6b6c6d6e6f6g6h6 = _mm512_unpacklo_pd(a6a7c6c7e6e7g6g7, b6b7d6d7f6f7h6h7);
    // a7,b7,c7,d7,e7,f7,g7,h7
    __m512d a7b7c7d7e7f7g7h7 = _mm512_unpackhi_pd(a6a7c6c7e6e7g6g7, b6b7d6d7f6f7h6h7);

    _mm512_store_pd(&out[pos[0]].var[0], a0b0c0d0e0f0g0h0);
    _mm512_store_pd(&out[pos[1]].var[0], a1b1c1d1e1f1g1h1);
    _mm512_store_pd(&out[pos[2]].var[0], a2b2c2d2e2f2g2h2);
    _mm512_store_pd(&out[pos[3]].var[0], a3b3c3d3e3f3g3h3);
    _mm512_store_pd(&out[pos[4]].var[0], a4b4c4d4e4f4g4h4);
    _mm512_store_pd(&out[pos[5]].var[0], a5b5c5d5e5f5g5h5);
    _mm512_store_pd(&out[pos[6]].var[0], a6b6c6d6e6f6g6h6);
    _mm512_store_pd(&out[pos[7]].var[0], a7b7c7d7e7f7g7h7);
}

# endif
