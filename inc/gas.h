# ifndef __GAS_H__
# define __GAS_H__

# include <sizes.h>
# include <var.h>


class cGas
{
private:
protected:
    int             nx,nvel,npde;
    double          rg,gam;
    double          eps;
    double          unit[3], deflt[3];

public:
    cGas();
    cGas(int, int, int);
    void       auxv(int, int, aos1x8_t *, aos1x8_t *);

    void       iflux(int, int, int *[], aos1x4_t *, aos1x8_t *, aos1x9_t *,
                     aos3x8_t *, aos1x8_t *, aos1x8_t *,
                     double *[], double *[], double *[], double *[]);

    inline __attribute__((always_inline))
    void       deltq(double [][VECLEN], double [][VECLEN], double [][VECLEN], double [][3][VECLEN],
                     double [][VECLEN], double [][VECLEN], double [][VECLEN], double [][3][VECLEN],
                     double [3][VECLEN], double [][VECLEN], double [][VECLEN], double [][VECLEN]);




};


# endif
