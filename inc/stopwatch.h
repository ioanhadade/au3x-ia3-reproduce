# ifndef __STOPWATCH_H__
# define __STOPWATCH_H__

# include <ctime>
# include <string>
# include <iostream>

class cStopwatch
{
private:
    std::string name;
    double elapsed;
    struct timespec previous;
    int count;

public:
    void reset();
    double record();
    double average()
    {
        return elapsed/static_cast<double>(count);
    };
    double total()
    {
        return elapsed;
    };

    cStopwatch(std::string);
    ~cStopwatch();

    friend std::ostream &operator<< (std::ostream &os, cStopwatch &sw)
    {
        os << "============"                                     << std::endl;
        os                                                       << std::endl;
        os <<                                 sw.name            << std::endl;
        os << "Time (s): "         << '\t' << sw.elapsed         << std::endl;
        os << "Average (s): "      << '\t' << sw.average()       << std::endl;
        os << "Call Count: "       << '\t' << sw.count           << std::endl;
        os                                                       << std::endl;

        return os;
    }
};

cStopwatch::cStopwatch(std::string _name):name(_name)
{
    elapsed=0.;
    count=0;
}

cStopwatch::~cStopwatch()
{
    elapsed=0.;
    count=0;
    name=" ";
}

void cStopwatch::reset()
{
    clock_gettime(CLOCK_MONOTONIC, &previous);
}

double cStopwatch::record()
{
    double diff;

    struct timespec current;
    clock_gettime(CLOCK_MONOTONIC, &current);
    diff = (current.tv_sec-previous.tv_sec + 1e-9*(current.tv_nsec - previous.tv_nsec));
    elapsed += diff;
    previous = current;
    ++count;
    return diff;
}

# endif
