# ifndef __DOMAIN_H__
# define __DOMAIN_H__

# include <sizes.h>
# include <var.h>
# include <gas.h>

class cDomain
{
protected:
    int             nq;                  // number of degrees of freedom 
    int             nx;                  // number of coordinates
    int             npde;                // number of unknowns 
    int             nfc;                 // number of interfaces
    int             naux;                // number of auxiliary variables
    int             nauxf;               // number of interface auxiliary variables
    int             nvel;                // number of velocities
    cGas            *fld;                // Gas object
    int             *sifq,*ifq[2];       // list of interface end-points -- face to cells mapper
    //  dof arrays
    aos1x8_t          *q;
    aos1x8_t          *aux;
    aos1x8_t          *daux;
    aos1x8_t          *rhs;
    aos1x4_t          *xq;
    aos1x9_t          *dxdx;
    aos3x8_t          *dqdx;
    // connection arrays
    double          *sauxf;
    double          **auxf;
    double          *swnc;
    double          *wnc[4];
    double          *sxc,*xc[3];
    double          *swxdc,*wxdc[1];
 
    // permutation and index arrays if reordered
    int             *iprm;
    int             *indx;
    // temporary buffer for work etc.
    double         *wrk[MAXNPDES];
public:

    cDomain(int, int, int, int, int, int, int);
    ~cDomain();

    void init(double, double, double, double, double, double, double);
    void load();
    void grad();
    void run_flux_kernel();
    void output_results();

    // grid renumbering
    void renumber_grid();
    // face reordering/colouring
    void colour_lohner1(int *[], int, int, int, int *);
    void colour_lohner2(int *[], int, int, int, int, int *);

};

# endif

