# ifndef __SIZES_H__
# define __SIZES_H__

# define MAXNEIGH 10
# define MAXNPDES 8
# define MAXAUXV  8

#if defined __MIC__
    #define VECLEN 8
    #define ALIGN  64
#elif defined __AVX512F__
    #define VECLEN 8
    #define ALIGN  64
#elif defined __AVX__
    #define VECLEN 4
    #define ALIGN 32
#else
    #define VECLEN 2
    #define ALIGN 16
#endif
# endif
