# ifndef __VECIMCI_H__
# define __VECIMCI_H__

#
// masks et al used for swizzles
const __mmask maskAAAA   = 0x0;
const __mmask maskBBBB   = 0xFF;
const __mmask8 maskABAA  = 0x22;
const __mmask8 maskAAAB  = 0x88;
const __mmask8 maskAABB  = 0xCC;
const __mmask8 maskABBB  = 0xEE;
const __mmask8 maskBBAB  = 0xBB;
const __mmask16 maskFF00 = 0xFF00;
const __mmask16 maskFF   = 0xFF;
const __mmask8 maskF     = 0xF;
const __mmask8 maskF0    = 0xF0;


inline __attribute__((always_inline)) void
load(double *data[], int nvar, int offset, double out[][VECLEN])
{
   int ivar;
   __m512d v;

   for( ivar=0;ivar<nvar;ivar++ )
   {
      v= _mm512_load_pd(&data[ivar][offset]);
      _mm512_store_pd(&out[ivar][0],v);
   }
}

inline __attribute__((always_inline)) void
load(double *data[], int offset, double out[VECLEN])
{
    __m512d v;

    v= _mm512_load_pd(&data[0][offset]);
    _mm512_store_pd(&out[0],v);
}

template < typename type > inline __attribute__((always_inline))
void gather1x7(type *data, int *pos, double out[][VECLEN])
{
    __m512d v[8];

    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0]= _mm512_load_pd(&data[pos[0]].var[0]); 
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1]= _mm512_load_pd(&data[pos[1]].var[0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2]= _mm512_load_pd(&data[pos[2]].var[0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3]= _mm512_load_pd(&data[pos[3]].var[0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4]= _mm512_load_pd(&data[pos[4]].var[0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5]= _mm512_load_pd(&data[pos[5]].var[0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6]= _mm512_load_pd(&data[pos[6]].var[0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7]= _mm512_load_pd(&data[pos[7]].var[0]);
 
 
    // a0,a1,a2,a3,e0,e1,e2,e3
    __m512d a0a1e0e1= _mm512_mask_blend_pd(maskABAA,v[0],_mm512_swizzle_pd(v[1],_MM_SWIZ_REG_CDAB));
    __m512d a2a3e2e3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[3],
                                           _MM_SWIZ_REG_AAAA));    
    __m512d a0a1a2a3e0e1e2e3= _mm512_mask_blend_pd(maskAABB,a0a1e0e1,a2a3e2e3);

    // b0,b1,b2,b3,f0,f1,f2,f3
    __m512d b0b1f0f1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_CDAB),v[1]);
    __m512d b2b3f2f3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[3],
                                           _MM_SWIZ_REG_BADC));
    __m512d b0b1b2b3f0f1f2f3= _mm512_mask_blend_pd(maskAABB,b0b1f0f1,b2b3f2f3);

    // c0,c1,c2,c3,g0,g1,g2,g3
    __m512d c0c1g0g1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[1],
                                           _MM_SWIZ_REG_DACB));
    __m512d c2c3g2g3= _mm512_mask_blend_pd(maskAAAB,v[2],_mm512_swizzle_pd(v[3],_MM_SWIZ_REG_CDAB));
    __m512d c0c1c2c3g0g1g2g3= _mm512_mask_blend_pd(maskAABB,c0c1g0g1,c2c3g2g3);

    // d0,d1,d2,d3,h0,h1,h2,h3
    __m512d d0d1h0h1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[1],
                                           _MM_SWIZ_REG_BADC));
    __m512d d2d3h2h3= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_CDAB),v[3]);
    __m512d d0d1d2d3h0h1h2h3= _mm512_mask_blend_pd(maskAABB,d0d1h0h1,d2d3h2h3);

    // a4,a5,a6,a7,e4,e5,e6,e7
    __m512d a4a5e4e5= _mm512_mask_blend_pd(maskABAA,v[4],_mm512_swizzle_pd(v[5],_MM_SWIZ_REG_CDAB));
    __m512d a6a7e6e7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[7],
                                           _MM_SWIZ_REG_AAAA));
    __m512d a4a5a6a7e4e5e6e7= _mm512_mask_blend_pd(maskAABB,a4a5e4e5,a6a7e6e7);

    // b4,b5,b6,b7,f4,f5,f6,f7
    __m512d b4b5f4f5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_CDAB),v[5]);
    __m512d b6b7f6f7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[7],
                                           _MM_SWIZ_REG_BADC));
    __m512d b4b5b6b7f4f5f6f7= _mm512_mask_blend_pd(maskAABB,b4b5f4f5,b6b7f6f7);

    // c4,c5,c6,c7,g4,g5,g6,g7
    __m512d c4c5g4g5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[5],
                                           _MM_SWIZ_REG_DACB));
    __m512d c6c7g6g7= _mm512_mask_blend_pd(maskAAAB,v[6],_mm512_swizzle_pd(v[7],_MM_SWIZ_REG_CDAB));
    __m512d c4c5c6c7g4g5g6g7= _mm512_mask_blend_pd(maskAABB,c4c5g4g5,c6c7g6g7);

    // d4,d5,d6,d7,h4,h5,h6,h7
    __m512d d4d5h4h5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[5],
                                           _MM_SWIZ_REG_BADC));
    __m512d d6d7h6h7= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_CDAB),v[7]);
    __m512d d4d5d6d7h4h5h6h7= _mm512_mask_blend_pd(maskAABB,d4d5h4h5,d6d7h6h7);

    // inter lane shuffles
    // a0,a1,a2,a3,a4,a5,a6,a7
    __m512d a0a1a2a3a4a5a6a7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a0a1a2a3e0e1e2e3), maskFF00,
                             _mm512_castpd_si512(a4a5a6a7e4e5e6e7),_MM_PERM_BABA));
    // e0,e1,e2,e3,e4,e5,e6,e7
    __m512d e0e1e2e3e4e5e6e7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a4a5a6a7e4e5e6e7), maskFF,
                             _mm512_castpd_si512(a0a1a2a3e0e1e2e3),_MM_PERM_DCDC));
    // b0,b1,b2,b3,b4,b5,b6,b7
    __m512d b0b1b2b3b4b5b6b7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b0b1b2b3f0f1f2f3), maskFF00,
                             _mm512_castpd_si512(b4b5b6b7f4f5f6f7),_MM_PERM_BABA));
    // f0,f1,f2,f3,f4,f5,f6,f7
    __m512d f0f1f2f3f4f5f6f7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b4b5b6b7f4f5f6f7), maskFF,
                             _mm512_castpd_si512(b0b1b2b3f0f1f2f3),_MM_PERM_DCDC));
    // c0,c1,c2,c3,c4,c5,c6,c7
    __m512d c0c1c2c3c4c5c6c7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c0c1c2c3g0g1g2g3), maskFF00,
                             _mm512_castpd_si512(c4c5c6c7g4g5g6g7),_MM_PERM_BABA));
    // g0,g1,g2,g3,g4,g5,g6,g7
    __m512d g0g1g2g3g4g5g6g7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c4c5c6c7g4g5g6g7), maskFF,
                             _mm512_castpd_si512(c0c1c2c3g0g1g2g3),_MM_PERM_DCDC));
    // e0,e1,e2,e3,e4,e5,e6,e7
    __m512d d0d1d2d3d4d5d6d7= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(d0d1d2d3h0h1h2h3), maskFF00,
                              _mm512_castpd_si512(d4d5d6d7h4h5h6h7),_MM_PERM_BABA));
    // h0,h1,h2,h3,h4,h5,h6,h7 -- not required.


    _mm512_store_pd(&out[0][0],a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0],b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0],c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][0],d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][0],e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][0],f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][0],g0g1g2g3g4g5g6g7);
}

template < typename type > inline __attribute__((always_inline))
void gather1x3(type *data, int *pos, double out[][VECLEN])
{
    // a0b0c00a0b0c00
    __m512d a0b0c0= _mm512_extload_pd(&data[pos[0]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    // a1b1c10a1b1d10   
    __m512d a1b1c1= _mm512_extload_pd(&data[pos[1]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    // a2b2c20a2b2c20
    __m512d a2b2c2= _mm512_extload_pd(&data[pos[2]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    // a3b3c30a3b3d30
    __m512d a3b3c3= _mm512_extload_pd(&data[pos[3]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    // a4b4c40a4b4c40
    __m512d a4b4c4= _mm512_extload_pd(&data[pos[4]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    // a5b5c50a5b5c50
    __m512d a5b5c5= _mm512_extload_pd(&data[pos[5]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    // a6b6c60a6b6c60
    __m512d a6b6c6= _mm512_extload_pd(&data[pos[6]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    // a7b7c70a7b7c70
    __m512d a7b7c7= _mm512_extload_pd(&data[pos[7]].var[0], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);

    // a0,a1,a2,a3,a4,a5,a6,a7
    __m512d a0a1= _mm512_mask_swizzle_pd(a0b0c0, _mm512_int2mask(0xEE), a1b1c1, _MM_SWIZ_REG_AAAA);
    __m512d a2a3= _mm512_mask_swizzle_pd(a2b2c2, _mm512_int2mask(0xEE), a3b3c3, _MM_SWIZ_REG_AAAA);
    __m512d a4a5= _mm512_mask_swizzle_pd(a4b4c4, _mm512_int2mask(0xEE), a5b5c5, _MM_SWIZ_REG_AAAA);
    __m512d a6a7= _mm512_mask_swizzle_pd(a6b6c6, _mm512_int2mask(0xEE), a7b7c7, _MM_SWIZ_REG_AAAA);

    __m512d a0a1a2a3= _mm512_mask_blend_pd(maskAABB,a0a1,_mm512_swizzle_pd(a2a3,_MM_SWIZ_REG_BADC));
    __m512d a4a5a6a7= _mm512_mask_blend_pd(maskAABB,a4a5,_mm512_swizzle_pd(a6a7,_MM_SWIZ_REG_BADC));
    __m512d a0a1a2a3a4a5a6a7= _mm512_mask_blend_pd(_mm512_int2mask(0xF0),a0a1a2a3,a4a5a6a7);

    // b0,b1,b2,b3,b4,b5,b6,b7
    __m512d b0b1= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a0b0c0,_MM_SWIZ_REG_CDAB), _mm512_int2mask(0xEE), a1b1c1,
                                       _MM_SWIZ_REG_BBBB);
    __m512d b2b3= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a2b2c2,_MM_SWIZ_REG_CDAB), _mm512_int2mask(0xEE), a3b3c3,
                                       _MM_SWIZ_REG_BBBB);
    __m512d b4b5= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a4b4c4,_MM_SWIZ_REG_CDAB), _mm512_int2mask(0xEE), a5b5c5,
                                       _MM_SWIZ_REG_BBBB);
    __m512d b6b7= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a6b6c6,_MM_SWIZ_REG_CDAB), _mm512_int2mask(0xEE), a7b7c7,
                                         _MM_SWIZ_REG_BBBB);
    __m512d b0b1b2b3= _mm512_mask_blend_pd(maskAABB,b0b1,_mm512_swizzle_pd(b2b3,_MM_SWIZ_REG_BADC));
    __m512d b4b5b6b7= _mm512_mask_blend_pd(maskAABB,b4b5,_mm512_swizzle_pd(b6b7,_MM_SWIZ_REG_BADC));
    __m512d b0b1b2b3b4b5b6b7= _mm512_mask_blend_pd(_mm512_int2mask(0xF0),b0b1b2b3,b4b5b6b7);

    // c0,c1,c2,c3,c4,c5,c6,c7
    __m512d c0c1= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a0b0c0,_MM_SWIZ_REG_BADC), _mm512_int2mask(0xEE), a1b1c1,
                                         _MM_SWIZ_REG_CCCC);
    __m512d c2c3= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a2b2c2,_MM_SWIZ_REG_BADC), _mm512_int2mask(0xEE), a3b3c3,
                                         _MM_SWIZ_REG_CCCC);
    __m512d c4c5= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a4b4c4,_MM_SWIZ_REG_BADC), _mm512_int2mask(0xEE), a5b5c5,
                                         _MM_SWIZ_REG_CCCC);
    __m512d c6c7= _mm512_mask_swizzle_pd(_mm512_swizzle_pd(a6b6c6,_MM_SWIZ_REG_BADC), _mm512_int2mask(0xEE), a7b7c7,
                                         _MM_SWIZ_REG_CCCC);
    __m512d c0c1c2c3= _mm512_mask_blend_pd(maskAABB,c0c1,_mm512_swizzle_pd(c2c3,_MM_SWIZ_REG_BADC));
    __m512d c4c5c6c7= _mm512_mask_blend_pd(maskAABB,c4c5,_mm512_swizzle_pd(c6c7,_MM_SWIZ_REG_BADC));
    __m512d c0c1c2c3c4c5c6c7= _mm512_mask_blend_pd(_mm512_int2mask(0xF0),c0c1c2c3,c4c5c6c7);
    
    _mm512_store_pd(&out[0][0],a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0],b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0],c0c1c2c3c4c5c6c7);
}

template < typename type > inline __attribute__((always_inline))
void gather3x7(type *data, int *pos, double out[][3][VECLEN])
{
    __m512d v[8];
    __m512d a0a1e0e1,a2a3e2e3,a0a1a2a3e0e1e2e3;
    __m512d b0b1f0f1,b2b3f2f3,b0b1b2b3f0f1f2f3;
    __m512d c0c1g0g1,c2c3g2g3,c0c1c2c3g0g1g2g3;
    __m512d d0d1h0h1,d2d3h2h3,d0d1d2d3h0h1h2h3;
    __m512d a4a5e4e5,a6a7e6e7,a4a5a6a7e4e5e6e7;
    __m512d b4b5f4f5,b6b7f6f7,b4b5b6b7f4f5f6f7;
    __m512d c4c5g4g5,c6c7g6g7,c4c5c6c7g4g5g6g7;
    __m512d d4d5h4h5,d6d7h6h7,d4d5d6d7h4h5h6h7;
    __m512d a0a1a2a3a4a5a6a7, b0b1b2b3b4b5b6b7;
    __m512d c0c1c2c3c4c5c6c7, d0d1d2d3d4d5d6d7;
    __m512d e0e1e2e3e4e5e6e7, f0f1f2f3f4f5f6f7;
    __m512d g0g1g2g3g4g5g6g7;

    // 1
    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0]= _mm512_load_pd(&data[pos[0]].var[0][0]); 
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1]= _mm512_load_pd(&data[pos[1]].var[0][0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2]= _mm512_load_pd(&data[pos[2]].var[0][0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3]= _mm512_load_pd(&data[pos[3]].var[0][0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4]= _mm512_load_pd(&data[pos[4]].var[0][0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5]= _mm512_load_pd(&data[pos[5]].var[0][0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6]= _mm512_load_pd(&data[pos[6]].var[0][0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7]= _mm512_load_pd(&data[pos[7]].var[0][0]);
 
 
   // a0,a1,a2,a3,e0,e1,e2,e3
    a0a1e0e1= _mm512_mask_blend_pd(maskABAA,v[0],_mm512_swizzle_pd(v[1],_MM_SWIZ_REG_CDAB));
    a2a3e2e3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[3],
                                   _MM_SWIZ_REG_AAAA));    
    a0a1a2a3e0e1e2e3= _mm512_mask_blend_pd(maskAABB,a0a1e0e1,a2a3e2e3);

    // b0,b1,b2,b3,f0,f1,f2,f3
    b0b1f0f1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_CDAB),v[1]);
    b2b3f2f3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[3],
                                   _MM_SWIZ_REG_BADC));
    b0b1b2b3f0f1f2f3= _mm512_mask_blend_pd(maskAABB,b0b1f0f1,b2b3f2f3);

    // c0,c1,c2,c3,g0,g1,g2,g3
    c0c1g0g1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[1],
                                   _MM_SWIZ_REG_DACB));
    c2c3g2g3= _mm512_mask_blend_pd(maskAAAB,v[2],_mm512_swizzle_pd(v[3],_MM_SWIZ_REG_CDAB));
    c0c1c2c3g0g1g2g3= _mm512_mask_blend_pd(maskAABB,c0c1g0g1,c2c3g2g3);

    // d0,d1,d2,d3,h0,h1,h2,h3
    d0d1h0h1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[1],
                                   _MM_SWIZ_REG_BADC));
    d2d3h2h3= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_CDAB),v[3]);
    d0d1d2d3h0h1h2h3= _mm512_mask_blend_pd(maskAABB,d0d1h0h1,d2d3h2h3);

    // a4,a5,a6,a7,e4,e5,e6,e7
    a4a5e4e5= _mm512_mask_blend_pd(maskABAA,v[4],_mm512_swizzle_pd(v[5],_MM_SWIZ_REG_CDAB));
    a6a7e6e7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[7],
                                   _MM_SWIZ_REG_AAAA));
    a4a5a6a7e4e5e6e7= _mm512_mask_blend_pd(maskAABB,a4a5e4e5,a6a7e6e7);

    // b4,b5,b6,b7,f4,f5,f6,f7
    b4b5f4f5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_CDAB),v[5]);
    b6b7f6f7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[7],
                                   _MM_SWIZ_REG_BADC));
    b4b5b6b7f4f5f6f7= _mm512_mask_blend_pd(maskAABB,b4b5f4f5,b6b7f6f7);

    // c4,c5,c6,c7,g4,g5,g6,g7
    c4c5g4g5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[5],
                                    _MM_SWIZ_REG_DACB));
    c6c7g6g7= _mm512_mask_blend_pd(maskAAAB,v[6],_mm512_swizzle_pd(v[7],_MM_SWIZ_REG_CDAB));
    c4c5c6c7g4g5g6g7= _mm512_mask_blend_pd(maskAABB,c4c5g4g5,c6c7g6g7);

    // d4,d5,d6,d7,h4,h5,h6,h7
    d4d5h4h5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[5],
                                   _MM_SWIZ_REG_BADC));
    d6d7h6h7= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_CDAB),v[7]);
    d4d5d6d7h4h5h6h7= _mm512_mask_blend_pd(maskAABB,d4d5h4h5,d6d7h6h7);

    // inter lane shuffles
    // a0,a1,a2,a3,a4,a5,a6,a7
    a0a1a2a3a4a5a6a7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a0a1a2a3e0e1e2e3), maskFF00,
                             _mm512_castpd_si512(a4a5a6a7e4e5e6e7),_MM_PERM_BABA));
    // e0,e1,e2,e3,e4,e5,e6,e7
    e0e1e2e3e4e5e6e7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a4a5a6a7e4e5e6e7), maskFF,
                             _mm512_castpd_si512(a0a1a2a3e0e1e2e3),_MM_PERM_DCDC));
    // b0,b1,b2,b3,b4,b5,b6,b7
    b0b1b2b3b4b5b6b7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b0b1b2b3f0f1f2f3), maskFF00,
                             _mm512_castpd_si512(b4b5b6b7f4f5f6f7),_MM_PERM_BABA));
    // f0,f1,f2,f3,f4,f5,f6,f7
    f0f1f2f3f4f5f6f7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b4b5b6b7f4f5f6f7), maskFF,
                             _mm512_castpd_si512(b0b1b2b3f0f1f2f3),_MM_PERM_DCDC));
    // c0,c1,c2,c3,c4,c5,c6,c7
    c0c1c2c3c4c5c6c7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c0c1c2c3g0g1g2g3), maskFF00,
                             _mm512_castpd_si512(c4c5c6c7g4g5g6g7),_MM_PERM_BABA));
    // g0,g1,g2,g3,g4,g5,g6,g7
    g0g1g2g3g4g5g6g7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c4c5c6c7g4g5g6g7), maskFF,
                             _mm512_castpd_si512(c0c1c2c3g0g1g2g3),_MM_PERM_DCDC));
    // e0,e1,e2,e3,e4,e5,e6,e7
    d0d1d2d3d4d5d6d7= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(d0d1d2d3h0h1h2h3), maskFF00,
                              _mm512_castpd_si512(d4d5d6d7h4h5h6h7),_MM_PERM_BABA));
    // h0,h1,h2,h3,h4,h5,h6,h7 -- not required.


    _mm512_store_pd(&out[0][0][0],a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0][0],b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0][0],c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][0][0],d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][0][0],e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][0][0],f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][0][0],g0g1g2g3g4g5g6g7);

    // 2
    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0]= _mm512_load_pd(&data[pos[0]].var[1][0]); 
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1]= _mm512_load_pd(&data[pos[1]].var[1][0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2]= _mm512_load_pd(&data[pos[2]].var[1][0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3]= _mm512_load_pd(&data[pos[3]].var[1][0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4]= _mm512_load_pd(&data[pos[4]].var[1][0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5]= _mm512_load_pd(&data[pos[5]].var[1][0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6]= _mm512_load_pd(&data[pos[6]].var[1][0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7]= _mm512_load_pd(&data[pos[7]].var[1][0]);
 
 
   // a0,a1,a2,a3,e0,e1,e2,e3
    a0a1e0e1= _mm512_mask_blend_pd(maskABAA,v[0],_mm512_swizzle_pd(v[1],_MM_SWIZ_REG_CDAB));
    a2a3e2e3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[3],
                                   _MM_SWIZ_REG_AAAA));    
    a0a1a2a3e0e1e2e3= _mm512_mask_blend_pd(maskAABB,a0a1e0e1,a2a3e2e3);

    // b0,b1,b2,b3,f0,f1,f2,f3
    b0b1f0f1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_CDAB),v[1]);
    b2b3f2f3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[3],
                                   _MM_SWIZ_REG_BADC));
    b0b1b2b3f0f1f2f3= _mm512_mask_blend_pd(maskAABB,b0b1f0f1,b2b3f2f3);

    // c0,c1,c2,c3,g0,g1,g2,g3
    c0c1g0g1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[1],
                                   _MM_SWIZ_REG_DACB));
    c2c3g2g3= _mm512_mask_blend_pd(maskAAAB,v[2],_mm512_swizzle_pd(v[3],_MM_SWIZ_REG_CDAB));
    c0c1c2c3g0g1g2g3= _mm512_mask_blend_pd(maskAABB,c0c1g0g1,c2c3g2g3);

    // d0,d1,d2,d3,h0,h1,h2,h3
    d0d1h0h1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[1],
                                   _MM_SWIZ_REG_BADC));
    d2d3h2h3= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_CDAB),v[3]);
    d0d1d2d3h0h1h2h3= _mm512_mask_blend_pd(maskAABB,d0d1h0h1,d2d3h2h3);

    // a4,a5,a6,a7,e4,e5,e6,e7
    a4a5e4e5= _mm512_mask_blend_pd(maskABAA,v[4],_mm512_swizzle_pd(v[5],_MM_SWIZ_REG_CDAB));
    a6a7e6e7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[7],
                                   _MM_SWIZ_REG_AAAA));
    a4a5a6a7e4e5e6e7= _mm512_mask_blend_pd(maskAABB,a4a5e4e5,a6a7e6e7);

    // b4,b5,b6,b7,f4,f5,f6,f7
    b4b5f4f5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_CDAB),v[5]);
    b6b7f6f7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[7],
                                   _MM_SWIZ_REG_BADC));
    b4b5b6b7f4f5f6f7= _mm512_mask_blend_pd(maskAABB,b4b5f4f5,b6b7f6f7);

    // c4,c5,c6,c7,g4,g5,g6,g7
    c4c5g4g5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[5],
                                    _MM_SWIZ_REG_DACB));
    c6c7g6g7= _mm512_mask_blend_pd(maskAAAB,v[6],_mm512_swizzle_pd(v[7],_MM_SWIZ_REG_CDAB));
    c4c5c6c7g4g5g6g7= _mm512_mask_blend_pd(maskAABB,c4c5g4g5,c6c7g6g7);

    // d4,d5,d6,d7,h4,h5,h6,h7
    d4d5h4h5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[5],
                                   _MM_SWIZ_REG_BADC));
    d6d7h6h7= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_CDAB),v[7]);
    d4d5d6d7h4h5h6h7= _mm512_mask_blend_pd(maskAABB,d4d5h4h5,d6d7h6h7);

    // inter lane shuffles
    // a0,a1,a2,a3,a4,a5,a6,a7
    a0a1a2a3a4a5a6a7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a0a1a2a3e0e1e2e3), maskFF00,
                             _mm512_castpd_si512(a4a5a6a7e4e5e6e7),_MM_PERM_BABA));
    // e0,e1,e2,e3,e4,e5,e6,e7
    e0e1e2e3e4e5e6e7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a4a5a6a7e4e5e6e7), maskFF,
                             _mm512_castpd_si512(a0a1a2a3e0e1e2e3),_MM_PERM_DCDC));
    // b0,b1,b2,b3,b4,b5,b6,b7
    b0b1b2b3b4b5b6b7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b0b1b2b3f0f1f2f3), maskFF00,
                             _mm512_castpd_si512(b4b5b6b7f4f5f6f7),_MM_PERM_BABA));
    // f0,f1,f2,f3,f4,f5,f6,f7
    f0f1f2f3f4f5f6f7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b4b5b6b7f4f5f6f7), maskFF,
                             _mm512_castpd_si512(b0b1b2b3f0f1f2f3),_MM_PERM_DCDC));
    // c0,c1,c2,c3,c4,c5,c6,c7
    c0c1c2c3c4c5c6c7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c0c1c2c3g0g1g2g3), maskFF00,
                             _mm512_castpd_si512(c4c5c6c7g4g5g6g7),_MM_PERM_BABA));
    // g0,g1,g2,g3,g4,g5,g6,g7
    g0g1g2g3g4g5g6g7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c4c5c6c7g4g5g6g7), maskFF,
                             _mm512_castpd_si512(c0c1c2c3g0g1g2g3),_MM_PERM_DCDC));
    // e0,e1,e2,e3,e4,e5,e6,e7
    d0d1d2d3d4d5d6d7= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(d0d1d2d3h0h1h2h3), maskFF00,
                              _mm512_castpd_si512(d4d5d6d7h4h5h6h7),_MM_PERM_BABA));
    // h0,h1,h2,h3,h4,h5,h6,h7 -- not required.


    _mm512_store_pd(&out[0][1][0],a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][1][0],b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][1][0],c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][1][0],d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][1][0],e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][1][0],f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][1][0],g0g1g2g3g4g5g6g7);

    // 3
    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0]= _mm512_load_pd(&data[pos[0]].var[2][0]); 
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1]= _mm512_load_pd(&data[pos[1]].var[2][0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2]= _mm512_load_pd(&data[pos[2]].var[2][0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3]= _mm512_load_pd(&data[pos[3]].var[2][0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4]= _mm512_load_pd(&data[pos[4]].var[2][0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5]= _mm512_load_pd(&data[pos[5]].var[2][0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6]= _mm512_load_pd(&data[pos[6]].var[2][0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7]= _mm512_load_pd(&data[pos[7]].var[2][0]);
 
 
   // a0,a1,a2,a3,e0,e1,e2,e3
    a0a1e0e1= _mm512_mask_blend_pd(maskABAA,v[0],_mm512_swizzle_pd(v[1],_MM_SWIZ_REG_CDAB));
    a2a3e2e3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[3],
                                   _MM_SWIZ_REG_AAAA));    
    a0a1a2a3e0e1e2e3= _mm512_mask_blend_pd(maskAABB,a0a1e0e1,a2a3e2e3);

    // b0,b1,b2,b3,f0,f1,f2,f3
    b0b1f0f1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_CDAB),v[1]);
    b2b3f2f3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[3],
                                   _MM_SWIZ_REG_BADC));
    b0b1b2b3f0f1f2f3= _mm512_mask_blend_pd(maskAABB,b0b1f0f1,b2b3f2f3);

    // c0,c1,c2,c3,g0,g1,g2,g3
    c0c1g0g1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[1],
                                   _MM_SWIZ_REG_DACB));
    c2c3g2g3= _mm512_mask_blend_pd(maskAAAB,v[2],_mm512_swizzle_pd(v[3],_MM_SWIZ_REG_CDAB));
    c0c1c2c3g0g1g2g3= _mm512_mask_blend_pd(maskAABB,c0c1g0g1,c2c3g2g3);

    // d0,d1,d2,d3,h0,h1,h2,h3
    d0d1h0h1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[1],
                                   _MM_SWIZ_REG_BADC));
    d2d3h2h3= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_CDAB),v[3]);
    d0d1d2d3h0h1h2h3= _mm512_mask_blend_pd(maskAABB,d0d1h0h1,d2d3h2h3);

    // a4,a5,a6,a7,e4,e5,e6,e7
    a4a5e4e5= _mm512_mask_blend_pd(maskABAA,v[4],_mm512_swizzle_pd(v[5],_MM_SWIZ_REG_CDAB));
    a6a7e6e7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[7],
                                   _MM_SWIZ_REG_AAAA));
    a4a5a6a7e4e5e6e7= _mm512_mask_blend_pd(maskAABB,a4a5e4e5,a6a7e6e7);

    // b4,b5,b6,b7,f4,f5,f6,f7
    b4b5f4f5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_CDAB),v[5]);
    b6b7f6f7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[7],
                                   _MM_SWIZ_REG_BADC));
    b4b5b6b7f4f5f6f7= _mm512_mask_blend_pd(maskAABB,b4b5f4f5,b6b7f6f7);

    // c4,c5,c6,c7,g4,g5,g6,g7
    c4c5g4g5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[5],
                                    _MM_SWIZ_REG_DACB));
    c6c7g6g7= _mm512_mask_blend_pd(maskAAAB,v[6],_mm512_swizzle_pd(v[7],_MM_SWIZ_REG_CDAB));
    c4c5c6c7g4g5g6g7= _mm512_mask_blend_pd(maskAABB,c4c5g4g5,c6c7g6g7);

    // d4,d5,d6,d7,h4,h5,h6,h7
    d4d5h4h5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[5],
                                   _MM_SWIZ_REG_BADC));
    d6d7h6h7= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_CDAB),v[7]);
    d4d5d6d7h4h5h6h7= _mm512_mask_blend_pd(maskAABB,d4d5h4h5,d6d7h6h7);

    // inter lane shuffles
    // a0,a1,a2,a3,a4,a5,a6,a7
    a0a1a2a3a4a5a6a7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a0a1a2a3e0e1e2e3), maskFF00,
                             _mm512_castpd_si512(a4a5a6a7e4e5e6e7),_MM_PERM_BABA));
    // e0,e1,e2,e3,e4,e5,e6,e7
    e0e1e2e3e4e5e6e7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a4a5a6a7e4e5e6e7), maskFF,
                             _mm512_castpd_si512(a0a1a2a3e0e1e2e3),_MM_PERM_DCDC));
    // b0,b1,b2,b3,b4,b5,b6,b7
    b0b1b2b3b4b5b6b7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b0b1b2b3f0f1f2f3), maskFF00,
                             _mm512_castpd_si512(b4b5b6b7f4f5f6f7),_MM_PERM_BABA));
    // f0,f1,f2,f3,f4,f5,f6,f7
    f0f1f2f3f4f5f6f7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b4b5b6b7f4f5f6f7), maskFF,
                             _mm512_castpd_si512(b0b1b2b3f0f1f2f3),_MM_PERM_DCDC));
    // c0,c1,c2,c3,c4,c5,c6,c7
    c0c1c2c3c4c5c6c7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c0c1c2c3g0g1g2g3), maskFF00,
                             _mm512_castpd_si512(c4c5c6c7g4g5g6g7),_MM_PERM_BABA));
    // g0,g1,g2,g3,g4,g5,g6,g7
    g0g1g2g3g4g5g6g7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c4c5c6c7g4g5g6g7), maskFF,
                             _mm512_castpd_si512(c0c1c2c3g0g1g2g3),_MM_PERM_DCDC));
    // e0,e1,e2,e3,e4,e5,e6,e7
    d0d1d2d3d4d5d6d7= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(d0d1d2d3h0h1h2h3), maskFF00,
                              _mm512_castpd_si512(d4d5d6d7h4h5h6h7),_MM_PERM_BABA));
    // h0,h1,h2,h3,h4,h5,h6,h7 -- not required.


    _mm512_store_pd(&out[0][2][0],a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][2][0],b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][2][0],c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][2][0],d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][2][0],e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][2][0],f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][2][0],g0g1g2g3g4g5g6g7);

}

template < typename type > inline __attribute__((always_inline))
void gather1x9(type *data, int *pos, double out[][VECLEN])
{
    __m512d v[8];

    // a0,b0,c0,d0,e0,f0,g0,h0
    v[0]= _mm512_load_pd(&data[pos[0]].var[0]); 
    // a1,b1,c1,d1,e1,f1,g1,h1
    v[1]= _mm512_load_pd(&data[pos[1]].var[0]);
    // a2,b2,c2,d2,e2,f2,g2,h2
    v[2]= _mm512_load_pd(&data[pos[2]].var[0]);
    // a3,b3,c3,d3,e3,f3,g3,h3
    v[3]= _mm512_load_pd(&data[pos[3]].var[0]);
    // a4,b4,c4,d4,e4,f4,g4,h4
    v[4]= _mm512_load_pd(&data[pos[4]].var[0]);
    // a5,b5,c5,d5,e5,f5,g5,h5
    v[5]= _mm512_load_pd(&data[pos[5]].var[0]);
    // a6,b6,c6,d6,e6,f6,g6,h6
    v[6]= _mm512_load_pd(&data[pos[6]].var[0]);
    // a7,b7,c7,d7,e7,f7,g7,h7
    v[7]= _mm512_load_pd(&data[pos[7]].var[0]);
 

    __m512d i0= _mm512_extload_pd(&data[pos[0]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    __m512d i1= _mm512_extload_pd(&data[pos[1]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    __m512d i2= _mm512_extload_pd(&data[pos[2]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    __m512d i3= _mm512_extload_pd(&data[pos[3]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    __m512d i4= _mm512_extload_pd(&data[pos[4]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    __m512d i5= _mm512_extload_pd(&data[pos[5]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    __m512d i6= _mm512_extload_pd(&data[pos[6]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);
    __m512d i7= _mm512_extload_pd(&data[pos[7]].var[8], _MM_UPCONV_PD_NONE, _MM_BROADCAST_4X16, _MM_HINT_NONE);

    __m512d i0i1= _mm512_mask_swizzle_pd(i0, _mm512_int2mask(0xEE), i1, _MM_SWIZ_REG_AAAA);
    __m512d i2i3= _mm512_mask_swizzle_pd(i2, _mm512_int2mask(0xEE), i3, _MM_SWIZ_REG_AAAA);
    __m512d i4i5= _mm512_mask_swizzle_pd(i4, _mm512_int2mask(0xEE), i5, _MM_SWIZ_REG_AAAA);
    __m512d i6i7= _mm512_mask_swizzle_pd(i6, _mm512_int2mask(0xEE), i7, _MM_SWIZ_REG_AAAA);
    __m512d i0i1i2i3= _mm512_mask_blend_pd(maskAABB,i0i1,_mm512_swizzle_pd(i2i3,_MM_SWIZ_REG_BADC));
    __m512d i4i5i6i7= _mm512_mask_blend_pd(maskAABB,i4i5,_mm512_swizzle_pd(i6i7,_MM_SWIZ_REG_BADC));
    __m512d i0i1i2i3i4i5i6i7= _mm512_mask_blend_pd(_mm512_int2mask(0xF0),i0i1i2i3,i4i5i6i7);

    // a0,a1,a2,a3,e0,e1,e2,e3
    __m512d a0a1e0e1= _mm512_mask_blend_pd(maskABAA,v[0],_mm512_swizzle_pd(v[1],_MM_SWIZ_REG_CDAB));
    __m512d a2a3e2e3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[3],
                                           _MM_SWIZ_REG_AAAA));    
    __m512d a0a1a2a3e0e1e2e3= _mm512_mask_blend_pd(maskAABB,a0a1e0e1,a2a3e2e3);

    // b0,b1,b2,b3,f0,f1,f2,f3
    __m512d b0b1f0f1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_CDAB),v[1]);
    __m512d b2b3f2f3= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[3],
                                           _MM_SWIZ_REG_BADC));
    __m512d b0b1b2b3f0f1f2f3= _mm512_mask_blend_pd(maskAABB,b0b1f0f1,b2b3f2f3);

    // c0,c1,c2,c3,g0,g1,g2,g3
    __m512d c0c1g0g1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[1],
                                           _MM_SWIZ_REG_DACB));
    __m512d c2c3g2g3= _mm512_mask_blend_pd(maskAAAB,v[2],_mm512_swizzle_pd(v[3],_MM_SWIZ_REG_CDAB));
    __m512d c0c1c2c3g0g1g2g3= _mm512_mask_blend_pd(maskAABB,c0c1g0g1,c2c3g2g3);

    // d0,d1,d2,d3,h0,h1,h2,h3
    __m512d d0d1h0h1= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[0],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[1],
                                           _MM_SWIZ_REG_BADC));
    __m512d d2d3h2h3= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[2],_MM_SWIZ_REG_CDAB),v[3]);
    __m512d d0d1d2d3h0h1h2h3= _mm512_mask_blend_pd(maskAABB,d0d1h0h1,d2d3h2h3);

    // a4,a5,a6,a7,e4,e5,e6,e7
    __m512d a4a5e4e5= _mm512_mask_blend_pd(maskABAA,v[4],_mm512_swizzle_pd(v[5],_MM_SWIZ_REG_CDAB));
    __m512d a6a7e6e7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[7],
                                           _MM_SWIZ_REG_AAAA));
    __m512d a4a5a6a7e4e5e6e7= _mm512_mask_blend_pd(maskAABB,a4a5e4e5,a6a7e6e7);

    // b4,b5,b6,b7,f4,f5,f6,f7
    __m512d b4b5f4f5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_CDAB),v[5]);
    __m512d b6b7f6f7= _mm512_mask_blend_pd(maskAAAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(v[7],
                                           _MM_SWIZ_REG_BADC));
    __m512d b4b5b6b7f4f5f6f7= _mm512_mask_blend_pd(maskAABB,b4b5f4f5,b6b7f6f7);

    // c4,c5,c6,c7,g4,g5,g6,g7
    __m512d c4c5g4g5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_BADC),_mm512_swizzle_pd(v[5],
                                           _MM_SWIZ_REG_DACB));
    __m512d c6c7g6g7= _mm512_mask_blend_pd(maskAAAB,v[6],_mm512_swizzle_pd(v[7],_MM_SWIZ_REG_CDAB));
    __m512d c4c5c6c7g4g5g6g7= _mm512_mask_blend_pd(maskAABB,c4c5g4g5,c6c7g6g7);

    // d4,d5,d6,d7,h4,h5,h6,h7
    __m512d d4d5h4h5= _mm512_mask_blend_pd(maskABBB,_mm512_swizzle_pd(v[4],_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(v[5],
                                           _MM_SWIZ_REG_BADC));
    __m512d d6d7h6h7= _mm512_mask_blend_pd(maskBBAB,_mm512_swizzle_pd(v[6],_MM_SWIZ_REG_CDAB),v[7]);
    __m512d d4d5d6d7h4h5h6h7= _mm512_mask_blend_pd(maskAABB,d4d5h4h5,d6d7h6h7);

    // inter lane shuffles
    // a0,a1,a2,a3,a4,a5,a6,a7
    __m512d a0a1a2a3a4a5a6a7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a0a1a2a3e0e1e2e3), maskFF00,
                             _mm512_castpd_si512(a4a5a6a7e4e5e6e7),_MM_PERM_BABA));
    // e0,e1,e2,e3,e4,e5,e6,e7
    __m512d e0e1e2e3e4e5e6e7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(a4a5a6a7e4e5e6e7), maskFF,
                             _mm512_castpd_si512(a0a1a2a3e0e1e2e3),_MM_PERM_DCDC));
    // b0,b1,b2,b3,b4,b5,b6,b7
    __m512d b0b1b2b3b4b5b6b7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b0b1b2b3f0f1f2f3), maskFF00,
                             _mm512_castpd_si512(b4b5b6b7f4f5f6f7),_MM_PERM_BABA));
    // f0,f1,f2,f3,f4,f5,f6,f7
    __m512d f0f1f2f3f4f5f6f7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(b4b5b6b7f4f5f6f7), maskFF,
                             _mm512_castpd_si512(b0b1b2b3f0f1f2f3),_MM_PERM_DCDC));
    // c0,c1,c2,c3,c4,c5,c6,c7
    __m512d c0c1c2c3c4c5c6c7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c0c1c2c3g0g1g2g3), maskFF00,
                             _mm512_castpd_si512(c4c5c6c7g4g5g6g7),_MM_PERM_BABA));
    // g0,g1,g2,g3,g4,g5,g6,g7
    __m512d g0g1g2g3g4g5g6g7=_mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(c4c5c6c7g4g5g6g7), maskFF,
                             _mm512_castpd_si512(c0c1c2c3g0g1g2g3),_MM_PERM_DCDC));
    // e0,e1,e2,e3,e4,e5,e6,e7
    __m512d d0d1d2d3d4d5d6d7= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(d0d1d2d3h0h1h2h3), maskFF00,
                              _mm512_castpd_si512(d4d5d6d7h4h5h6h7),_MM_PERM_BABA));
    // h0,h1,h2,h3,h4,h5,h6,h7
    __m512d h0h1h2h3h4h5h6h7= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(d4d5d6d7h4h5h6h7), maskFF,
                              _mm512_castpd_si512(d0d1d2d3h0h1h2h3),_MM_PERM_DCDC));


    _mm512_store_pd(&out[0][0],a0a1a2a3a4a5a6a7);
    _mm512_store_pd(&out[1][0],b0b1b2b3b4b5b6b7);
    _mm512_store_pd(&out[2][0],c0c1c2c3c4c5c6c7);
    _mm512_store_pd(&out[3][0],d0d1d2d3d4d5d6d7);
    _mm512_store_pd(&out[4][0],e0e1e2e3e4e5e6e7);
    _mm512_store_pd(&out[5][0],f0f1f2f3f4f5f6f7);
    _mm512_store_pd(&out[6][0],g0g1g2g3g4g5g6g7);
    _mm512_store_pd(&out[7][0],h0h1h2h3h4h5h6h7);
    _mm512_store_pd(&out[8][0],i0i1i2i3i4i5i6i7);
}


template < typename type > inline __attribute__((always_inline))
void scatter1x7(double data[][VECLEN], int *pos, type *out)
{
    __m512d v[8];
      
    // a0,a1,a2,a3,a4,a5,a6,a7
    v[0]= _mm512_load_pd(&data[0][0]);
    // b0,b1,b2,b3,b4,b5,b6,b7
    v[1]= _mm512_load_pd(&data[1][0]); 
    // c0,c1,c2,c3,c4,c5,c6,c7
    v[2]= _mm512_load_pd(&data[2][0]);
    // d0,d1,d2,d3,d4,d5,d6,d7
    v[3]= _mm512_load_pd(&data[3][0]);
    // e0,e1,e2,e3,e4,e5,e6,e7
    v[4]= _mm512_load_pd(&data[4][0]);
    // f0,f1,f2,f3,f4,f5,f6,f7
    v[5]= _mm512_load_pd(&data[5][0]);
    // g0,g1,g2,g3,g4,g5,g6,g7
    v[6]= _mm512_load_pd(&data[6][0]);
    // not required
    v[7]= _mm512_set1_pd(0.); 

    // inter lane shuffles
    __m512d a03e03= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[0]), maskFF00, _mm512_castpd_si512(v[4]),
                                        _MM_PERM_BABA));
    __m512d a47e47= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[4]), maskFF, _mm512_castpd_si512(v[0]),
                                        _MM_PERM_DCDC));
    __m512d b03f03= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[1]), maskFF00, _mm512_castpd_si512(v[5]),
                                        _MM_PERM_BABA));
    __m512d b47f47= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[5]), maskFF, _mm512_castpd_si512(v[1]),
                                        _MM_PERM_DCDC));
    __m512d c03g03= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[2]), maskFF00,
                                        _mm512_castpd_si512(v[6]),_MM_PERM_BABA));
    __m512d c47g47= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[6]), maskFF,
                                        _mm512_castpd_si512(v[2]),_MM_PERM_DCDC));
    __m512d d03h03= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[3]), maskFF00, _mm512_castpd_si512(v[7]),
                                        _MM_PERM_BABA));
    __m512d d47h47= _mm512_castsi512_pd(_mm512_mask_permute4f128_epi32(_mm512_castpd_si512(v[7]), maskFF, _mm512_castpd_si512(v[3]),
                                        _MM_PERM_DCDC));


    // a0,b0,c0,d0,e0,f0,g0,h0
    __m512d a0b0e0f0= _mm512_mask_blend_pd(maskABBB, a03e03, _mm512_swizzle_pd(b03f03,_MM_SWIZ_REG_AAAA));
    __m512d c0d0g0h0= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c03g03,_MM_SWIZ_REG_AAAA),_mm512_swizzle_pd(d03h03,
                                           _MM_SWIZ_REG_AAAA));
    __m512d a0b0c0d0e0f0g0h0= _mm512_mask_blend_pd(maskAABB,a0b0e0f0,c0d0g0h0);

    // a1,b1,c1,d1,e1,f1,g1,h1
    __m512d a1b1e1f1= _mm512_mask_blend_pd(maskABBB, _mm512_swizzle_pd(a03e03,_MM_SWIZ_REG_BBBB), _mm512_swizzle_pd(b03f03,
                                           _MM_SWIZ_REG_BBBB));
    __m512d c1d1g1h1= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c03g03,_MM_SWIZ_REG_BBBB), _mm512_swizzle_pd(d03h03,
                                           _MM_SWIZ_REG_BBBB));
    __m512d a1b1c1d1e1f1g1h1= _mm512_mask_blend_pd(maskAABB,a1b1e1f1,c1d1g1h1);

     // a2,b2,c2,d2,e2,f2,g2,h2
    __m512d a2b2e2f2= _mm512_mask_blend_pd(maskABBB, _mm512_swizzle_pd(a03e03,_MM_SWIZ_REG_CCCC), _mm512_swizzle_pd(b03f03,
                                           _MM_SWIZ_REG_CCCC));
    __m512d c2d2g2h2= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c03g03,_MM_SWIZ_REG_CCCC), _mm512_swizzle_pd(d03h03,
                                           _MM_SWIZ_REG_CCCC));
    __m512d a2b2c2d2e2f2g2h2= _mm512_mask_blend_pd(maskAABB,a2b2e2f2,c2d2g2h2);

    // a3,b3,c3,d3,e3,f3,g3,h3
    __m512d a3b3e3f3= _mm512_mask_blend_pd(maskABBB, _mm512_swizzle_pd(a03e03,_MM_SWIZ_REG_DDDD), _mm512_swizzle_pd(b03f03,
                                           _MM_SWIZ_REG_DDDD));
    __m512d c3d3g3h3= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c03g03,_MM_SWIZ_REG_DDDD), _mm512_swizzle_pd(d03h03,
                                           _MM_SWIZ_REG_DDDD));
    __m512d a3b3c3d3e3f3g3h3= _mm512_mask_blend_pd(maskAABB,a3b3e3f3,c3d3g3h3);

    // a4,b4,c4,d4,e4,f4,g4,h4
    __m512d a4b4e4f4= _mm512_mask_blend_pd(maskABBB, a47e47, _mm512_swizzle_pd(b47f47,_MM_SWIZ_REG_AAAA));
    __m512d c4d4g4h4= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c47g47,_MM_SWIZ_REG_AAAA),_mm512_swizzle_pd(d47h47,
                                           _MM_SWIZ_REG_AAAA));
    __m512d a4b4c4d4e4f4g4h4= _mm512_mask_blend_pd(maskAABB,a4b4e4f4,c4d4g4h4);

    // a5,b5,c5,d5,e5,f5,g5,h5
    __m512d a5b5e5f5= _mm512_mask_blend_pd(maskABBB, _mm512_swizzle_pd(a47e47,_MM_SWIZ_REG_BBBB), _mm512_swizzle_pd(b47f47,
                                           _MM_SWIZ_REG_BBBB));
    __m512d c5d5g5h5= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c47g47,_MM_SWIZ_REG_BBBB),_mm512_swizzle_pd(d47h47,
                                           _MM_SWIZ_REG_BBBB));
    __m512d a5b5c5d5e5f5g5h5= _mm512_mask_blend_pd(maskAABB,a5b5e5f5,c5d5g5h5);

    // a6,b6,c6,d6,e6,f6,g6,h6
    __m512d a6b6e6f6= _mm512_mask_blend_pd(maskABBB, _mm512_swizzle_pd(a47e47,_MM_SWIZ_REG_CCCC), _mm512_swizzle_pd(b47f47,
                                           _MM_SWIZ_REG_CCCC));
    __m512d c6d6g6h6= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c47g47,_MM_SWIZ_REG_CCCC),_mm512_swizzle_pd(d47h47,
                                           _MM_SWIZ_REG_CCCC));
    __m512d a6b6c6d6e6f6g6h6= _mm512_mask_blend_pd(maskAABB,a6b6e6f6,c6d6g6h6);

    // a7,b7,c7,d7,e7,f7,g7,h7
    __m512d a7b7e7f7= _mm512_mask_blend_pd(maskABBB, _mm512_swizzle_pd(a47e47,_MM_SWIZ_REG_DDDD), _mm512_swizzle_pd(b47f47,
                                           _MM_SWIZ_REG_DDDD));
    __m512d c7d7g7h7= _mm512_mask_blend_pd(maskAAAB, _mm512_swizzle_pd(c47g47,_MM_SWIZ_REG_DDDD),_mm512_swizzle_pd(d47h47,
                                           _MM_SWIZ_REG_DDDD));
    __m512d a7b7c7d7e7f7g7h7= _mm512_mask_blend_pd(maskAABB,a7b7e7f7,c7d7g7h7);
    // store result
    _mm512_store_pd(&out[pos[0]].var[0],a0b0c0d0e0f0g0h0);
    _mm512_store_pd(&out[pos[1]].var[0],a1b1c1d1e1f1g1h1);
    _mm512_store_pd(&out[pos[2]].var[0],a2b2c2d2e2f2g2h2);
    _mm512_store_pd(&out[pos[3]].var[0],a3b3c3d3e3f3g3h3);
    _mm512_store_pd(&out[pos[4]].var[0],a4b4c4d4e4f4g4h4);
    _mm512_store_pd(&out[pos[5]].var[0],a5b5c5d5e5f5g5h5);
    _mm512_store_pd(&out[pos[6]].var[0],a6b6c6d6e6f6g6h6);
    _mm512_store_pd(&out[pos[7]].var[0],a7b7c7d7e7f7g7h7);
}

# endif
