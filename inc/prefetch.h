# ifndef __PREFETCH_H__
# define __PREFETCH_H__

# include <immintrin.h>
# include <var.h>

// Known optimal values.

// SKX
/*
# define ENABLE_PF
# define L2_INDEX_PF 16
# define L1_DATA_PF_LIN 32
# define L2_DATA_PF_LIN 64
*/
// KNL
/*
# define ENABLE_PF
# define L2_INDEX_PF 32
# define L1_DATA_PF_LIN 32
# define L2_DATA_PF_LIN 64
*/
// KNC (1 thread per core)
/*
# define ENABLE_PF
# define L1_INDEX_PF 8
# define L2_INDEX_PF 64
# define L1_DATA_PF_LIN 32
# define L2_DATA_PF_LIN 64
*/

// KNC (4 threads per core)
/*
# define ENABLE_PF
# define L1_INDEX_PF 16
# define L2_INDEX_PF 64
# define L1_DATA_PF_LIN 32
# define L2_DATA_PF_LIN 64
*/


// if prefetch distance for the index is defined
// the distance for prefetching the data is twice this.
// the distance for prefetching for contiguous loads
// is the same as the distance for prefetching the irregular data
# if defined L1_INDEX_PF
    # define L1_DATA_PF (L1_INDEX_PF >> 1)
    # define L1_DATA_PF_LIN L1_DATA_PF
# endif

# if defined L2_INDEX_PF
    # define L2_DATA_PF (L2_INDEX_PF >> 1)
    # define L2_DATA_PF_LIN L2_DATA_PF
# endif



inline __attribute__((always_inline)) void prefetchi(int *pos1, int *pos2)
{
#ifdef ENABLE_PF
    # if defined L2_INDEX_PF
    _mm_prefetch((char *) &(pos1[L2_INDEX_PF]),_MM_HINT_T1);
    _mm_prefetch((char *) &(pos2[L2_INDEX_PF]),_MM_HINT_T1);
    # endif
    # if defined L1_INDEX_PF
    _mm_prefetch((char *) &(pos1[L1_INDEX_PF]),_MM_HINT_T0);
    _mm_prefetch((char *) &(pos2[L1_INDEX_PF]),_MM_HINT_T0);
    # endif
#endif
}

template < typename type > inline __attribute__((always_inline)) 
void prefetchd(type *data, int *pos)
{
    int i;
    # if defined L2_INDEX_PF
    for( i=0;i<VECLEN;i++ )
    {
        _mm_prefetch((char *) &(data[pos[L2_DATA_PF+i]].var[0]), _MM_HINT_T1);
    }
    # endif
    # if defined L1_INDEX_PF
    for( i=0;i<VECLEN;i++ )
    {
        _mm_prefetch((char *) &(data[pos[L1_DATA_PF+i]].var[0]), _MM_HINT_T0);
    }
    # endif
}

template < typename type > inline __attribute__((always_inline)) 
void prefetchd(type *data, int *pos, int j)
{
    int i;
    # if defined L2_INDEX_PF
    for( i=0;i<VECLEN;i++ )
    {
        _mm_prefetch((char *) &(data[pos[L2_DATA_PF+i]].var[j][0]), _MM_HINT_T1);
    }
    # endif
    # if defined L1_INDEX_PF
    for( i=0;i<VECLEN;i++ )
    {
        _mm_prefetch((char *) &(data[pos[L1_DATA_PF+i]].var[j][0]), _MM_HINT_T0);
    }
    # endif
}


template < typename type > inline __attribute__((always_inline)) 
void prefetchd(type *data, int nvar, int offset)
{
    int ivar;
#ifdef ENABLE_PF
    for( ivar=0;ivar<nvar;ivar++ )
    {
    # if defined L2_DATA_PF_LIN
        _mm_prefetch((char *) &(data[ivar][offset+L2_DATA_PF_LIN]),_MM_HINT_T1);
    # endif
    # if defined L1_DATA_PF_LIN
        _mm_prefetch((char *) &(data[ivar][offset+L1_DATA_PF_LIN]),_MM_HINT_T0);
    # endif
    }
#endif
}

# endif
