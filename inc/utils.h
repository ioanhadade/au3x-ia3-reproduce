# ifndef __UTILS_H__
# define __UTILS_H__

# include <sstream>
# include <fstream>
# include <iostream>

template <typename type > 
void swap(type *v0, type *v1)
{
    type dum;
    dum= *v0;
    *v0= *v1;
    *v1= dum;
};

template < typename type > 
void setv(int ist, int ien, type val, type *data)
{
    int i;
    for( i=ist;i<ien;i++ )
    {
        data[i]= val;
    }
};

template < typename type1, typename type2 > 
void setv(int n0, int n1, int m0, int m1, type1 val, type2 *data)
{
    int i,j;
    for( i=n0;i<n1;i++ )
    {
        for( j=m0;j<m1;j++ )
        {
            data[i].var[j]= val;
        }
    }
};

template < typename type > 
void setv(int ist, int ien, int n, type val, type *data[])
{
    int i;
    for( i=0;i<n;i++ )
    {
        setv(ist,ien, val,data[i]);
    }
};


template < typename type > 
void reverse(int n, type *data)
{
    int i;
    for( i=0;i<n/2;i++ )
    {
        swap(data+i,data+(n-i-1));
    }
};

template <typename type > 
void identv(int n, type *ival)
{
    int i;
    for( i=0;i<n;i++ )
    {
        ival[i]=(type)i;
    }
};

template < typename type > 
void sift_down(int n, type *val, int *iprm, int l, int r)
{
    int j,jold,ia;
    type a;
    ia= iprm[l];
    a=val[iprm[l]];
    jold=l;
    j=2*l+1;
    while(j <= r)
    {
        if(j < r && val[iprm[j]] < val[iprm[j+1]]) j++;
        if(a >= val[iprm[j]]) break;
        iprm[jold]= iprm[j];
        jold=j;
        j=2*j+1;
    }
    iprm[jold]= ia;
};

template < typename type > 
void bsort(int n, type *val, int *iprm)
{
    int         i,j;
    identv(n,iprm);
    for( j=0;j<n-1;j++ )
    {
        for( i=j+1;i<n;i++ )
        {
            if(val[iprm[i]] < val[iprm[j]])
            {
                swap(&(iprm[i]),&(iprm[j]));
            }
        }
    }
};

template < typename type > 
void hsort(int n, type *val, int *iprm)
{
    int i;
    identv(n,iprm);
    for( i=n/2-1;i>=0;i-- )
    {
        sift_down(n,val,iprm,i,n-1);
    }
    for( i=n-1;i>0;i-- )
    {
        swap(&(iprm[0]), &(iprm[i]));
        sift_down(n,val,iprm,0,i-1);
    }
};

template < typename type > 
void permute(int n, int m, type *data, int *iprm)
{
    int i,j,k;
    type *buf;
    buf= new type[n];
# if defined __INTEL_COMPILER
    #pragma novector
# endif
    for( j=0;j<n;j++ )
    {
        i= iprm[j];
# if defined __INTEL_COMPILER
        #pragma novector
# endif
        for( k=0;k<m;k++ )
        {
            buf[j].var[k]= data[i].var[k];
        }
    }
# if defined __INTEL_COMPILER
    #pragma novector
# endif
    for( i=0;i<n;i++ )
    {
# if defined __INTEL_COMPILER
        #pragma novector
# endif
        for( k=0;k<m;k++ )
        {
            data[i].var[k]= buf[i].var[k];
        }
    }
    delete[] buf;
};


template < typename type > 
void permute(int n, type *data, int *iprm)
{
    int i,j;
    type *var;
    var= new type[n];
    for( j=0;j<n;j++ )
    {
        i= iprm[j];
        var[j]= data[i];
    }
    for( i=0;i<n;i++ )
    {
        data[i]= var[i];
    }
    delete[] var;
};


template < typename type > 
void relabel(int n, type *data, int *indx)
{
    int i,j;
    type *var;
    var= new type[n];
    for( i=0;i<n;i++ )
    {
        var[i]= indx[data[i]];
    }
    for( j=0;j<n;j++ )
    {
        data[j]= var[j];
    }
    delete[] var;
};

template < typename type > inline 
void subv(int n1, int n2, type *a, type *as[])
{
    int i;
    as[0]= a;
    for( i=1;i<n1;i++ )
    {
        as[i]= as[i-1]+n2;
    }
};

template < typename type > inline 
void subv(int n1, int n2, int n3, type *a, type *as[])
{
    int i;
    as[0]= a+n3;
    for( i=1;i<n1;i++ )
    {
        as[i]= as[i-1]+n2;
    }
};

template < typename type > 
void read_file(int nvar, int size, type *var[], std::string varname)
{
    int ivar;
    std::ifstream f;
    std::stringstream ss;
    std::string filename;
    for( ivar=0;ivar<nvar;ivar++ )
    {
        ss << varname.c_str() << ivar << ".bin";
        filename = ss.str();
        f.open(filename.c_str(), std::ios::binary | std::ios::in);
        if(f.good())
        {
            f.read((char *) var[ivar],sizeof(type) * size);
            f.close();
        }
        else
        {
            std::cout << "Error: Unable to open file: " << filename << std::endl;
        }
        ss.str("");
    }
};

template < typename type > 
void read_file(int nvar, int size, type *var, std::string varname)
{
    int ivar;
    std::ifstream f;
    std::stringstream ss;
    std::string filename;
    for( ivar=0;ivar<nvar;ivar++ )
    {
        ss << varname.c_str() << ivar << ".bin";
        filename = ss.str();
        f.open(filename.c_str(), std::ios::binary | std::ios::in);
        if(f.good())
        {
            f.read((char *) var[ivar],sizeof(type) * size);
            f.close();
        }
        else
        {
            std::cout << "Error: Unable to open file: " << filename << std::endl;
        }
        ss.str("");
    }
};

template < typename type1, typename type2 > 
void transpose(int nvar, int size, type1 *soa[], type2 *aos)
{
    int ivar, ic;
    for( ic=0;ic<size;ic++ )
    {
        for( ivar=0;ivar<nvar;ivar++ )
        {
            aos[ic].var[ivar]= soa[ivar][ic];
        }
    }
};

template < typename type > 
void write_text_file(int nvar, int size, type *var[], std::string varname)
{
    int ivar, ic;
    std::ofstream f;
    f.setf(std::ios_base::scientific);
    f.precision(15);
    f.open(varname.c_str());
    for( ic=0;ic<size;ic++ )
    {
        for( ivar=0;ivar<nvar;ivar++ )
        {
            f << var[ivar][ic] << " ";
        }
        f << std::endl;
    }
    f.close();
};

template < typename type > 
void write_text_file(int nvar, int size, type *var, std::string varname)
{
    int ivar, ic;
    std::ofstream f;
    f.setf(std::ios_base::scientific);
    f.width(20);
    f.precision(20);
    f.open(varname.c_str());
    for( ic=0;ic<size;ic++ )
    {
        for( ivar=0;ivar<nvar;ivar++ )
        {
            f << var[ic].var[ivar] << " ";
        }
        f << std::endl;
    }
    f.close();
};

inline int pad_double(int len, int align)
{
    return ((len*sizeof(double)+(align-1))/align)*(align/sizeof(double));
}

inline int pad_int(int len, int align)
{
    return ((len*sizeof(int)+(align-1))/align)*(align/sizeof(int));
}

inline void get_vec_loop_iter(int istart, int ifinish, int *istart0, int *ifinish0, int *istart1,
                              int *ifinish1)
{
    int looplen, rem;
    looplen= ifinish-istart;
    rem= looplen%VECLEN;
    if(rem==0)
    {
        *istart0= istart;
        *ifinish0= ifinish-(2*VECLEN);
        *istart1= *ifinish0;
        *ifinish1= ifinish;
    }
    else
    {
        *istart0= istart;
        *ifinish0= (ifinish-rem)-(2*VECLEN);
        *istart1= *ifinish0;
        *ifinish1= ifinish;
    }
}

# endif
