default:
	@echo ""
	@echo ""
	@echo "Options for make:"
	@echo "      make {version} TARGET={ARCH}"
	@echo "where version is one of the following:"
	@echo "============================="
	@echo "      swpf          - version with software prefetching enabled."
	@echo "      noswpf        - version with software prefetching disabled."
	@echo "      all           - both versions."
	@echo "and ARCH is one of the following:"
	@echo "============================="
	@echo "      SNB:          - Sandy Bridge microarchitecture."
	@echo "      BDW:          - Broadwell microarchitecture."
	@echo "      SKX:          - Skylake Server microarchitecture."
	@echo "      KNC:          - Knights Corner microarchitecture."
	@echo "      KNL:          - Knights Landing microarchitecture."
	@echo ""
	@echo "with default value being set to the Sandy Bridge architecture (SNB)."
	@echo "=============================="
	@echo "e.g.  make  swpf  TARGET=KNL"
	@echo "will build the executable for the KNL microarchitecture using"
	@echo "the best known parameters for software prefetching." 
	@echo ""

ifeq ($(TARGET),SNB)
  ARCH= -mavx
  L1_INDEX_PF=16
  L2_INDEX_PF=0
else ifeq ($(TARGET),BDW)
  ARCH= -xCORE-AVX2
  L1_INDEX_PF=8
  L2_INDEX_PF=0
else ifeq ($(TARGET),SKX)
  ARCH= -xCORE-AVX512
  L1_INDEX_PF=0
  L2_INDEX_PF=16
else ifeq ($(TARGET),KNC)
  ARCH= -mmic
  L1_INDEX_PF=0
  L2_INDEX_PF=64
else ifeq ($(TARGET),KNL)
  ARCH= -xMIC-AVX512
  L1_INDEX_PF=0
  L2_INDEX_PF=32
else
  ARCH= -mavx
  L1_INDEX_PF=16
  L2_INDEX_PF=0
endif

all: swpf noswpf

swpf:
	cd src/;                               \
	rm -f *.o;                             \
	make VER=swpf ARCH=$(ARCH) ENABLE_PF=1 \
	L1_INDEX_PF=$(L1_INDEX_PF)             \
	L2_INDEX_PF=$(L2_INDEX_PF);            \
	cd ..;

noswpf:
	cd src/;                               \
	rm -rf *.o;                            \
	make VER=noswpf ARCH=$(ARCH);          \
	cd ..;

clean:
	cd src/; make clean; cd ..;
