# include <iostream>
# include <immintrin.h>
# include <stdlib.h>
# include <domain.h>
# include <utils.h>

cDomain::cDomain(int nq, int nfc, int nx, int nvel, int npde, int naux, int nauxf):
    nq(nq), nfc(nfc), nx(nx), nvel(nvel), npde(npde), naux(naux), nauxf(nauxf)
{
    int ipde;
    int nq0, nfc0, nfc1;
    int i,j,k;

    // get new sizes that include padding
    // no need for padding at the end for AOS
    // padding is required within each short structure
    nq0=   pad_double(nq,  ALIGN);
    nfc0=  pad_double(nfc, ALIGN);
    nfc1=  pad_int(nfc, ALIGN);

    // allocate gas object
    fld= new cGas(nx, nvel, npde);

    q    = (aos1x8_t *) _mm_malloc(sizeof(aos1x8_t)*nq, ALIGN);
    rhs  = (aos1x8_t *) _mm_malloc(sizeof(aos1x8_t)*nq, ALIGN);
    daux = (aos1x8_t *) _mm_malloc(sizeof(aos1x8_t)*nq, ALIGN);
    aux  = (aos1x8_t *) _mm_malloc(sizeof(aos1x8_t)*nq, ALIGN);
    xq   = (aos1x4_t *) _mm_malloc(sizeof(aos1x4_t)*nq, ALIGN);
    dxdx = (aos1x9_t *) _mm_malloc(sizeof(aos1x9_t)*nq, ALIGN);
    dqdx = (aos3x8_t *) _mm_malloc(sizeof(aos3x8_t)*nq, ALIGN);

    // initialize arrays
    setv(0, nq, 0, MAXNPDES, 0., q);
    setv(0, nq, 0, MAXNPDES, 0., rhs);
    setv(0, nq, 0, MAXNPDES, 0., daux);
    setv(0, nq, 0, MAXNPDES, 0., aux);

    // initialize gradients
    for( i=0;i<nq;i++ )
    {
        for( j=0;j<3;j++ )
        {
            for( k=0;k<MAXNPDES;k++ )
            {
                dqdx[i].var[j][k]= 0.;
            }
        }
    }

    // allocate interface data structures
    swnc  = (double *) _mm_malloc(sizeof(double *)*(nx+1)*nfc0, ALIGN);  subv(nx+1, nfc0, swnc, wnc);
    setv(0, nfc0, nx+1, 0., wnc);

    sifq  = (int *) _mm_malloc(sizeof(int *)*2*nfc1, ALIGN);             subv(2, nfc1, sifq, ifq);

    sxc   = (double *) _mm_malloc(sizeof(double *)*nx*nfc0, ALIGN);      subv(nx, nfc0, sxc, xc);
    setv(0, nfc0, nx, 0., xc);

    swxdc = (double *) _mm_malloc(sizeof(double *)*1*nfc0, ALIGN);       subv(1, nfc0, swxdc,    wxdc);
    setv(0, nfc0,  1, 0., wxdc);

    // allocate interface data structures
    swnc  = (double *) _mm_malloc(sizeof(double *)*(nx+1)*nfc0, ALIGN);  subv(nx+1, nfc0, swnc, wnc);
    setv(0, nfc0, nx+1, 0., wnc);

    sifq  = (int *) _mm_malloc(sizeof(int *)*2*nfc1, ALIGN);             subv(2, nfc1, sifq, ifq);

    auxf=   (double **) _mm_malloc(sizeof(double **)*nauxf, ALIGN);
    sauxf = (double *) _mm_malloc(sizeof(double *)*nauxf*nfc0, ALIGN);   subv(nauxf, nfc0, sauxf, auxf);
    setv(0, nfc0, nauxf, 0., auxf);

    sxc   = (double *) _mm_malloc(sizeof(double *)*nx*nfc0, ALIGN);      subv(nx, nfc0, sxc, xc);
    setv(0, nfc0, nx, 0., xc);

    swxdc = (double *) _mm_malloc(sizeof(double *)*1*nfc0, ALIGN);       subv(1, nfc0, swxdc,    wxdc);
    setv(0, nfc0,  1, 0., wxdc);
};

void cDomain::init(double ux, double uv, double uw, double t, double p, double k, double omega)
{
    int iq,ipde;

    // initialise solution
    for( iq=0;iq<nq;iq++ )
    {
        q[iq].var[0]= ux;
        q[iq].var[1]= uv;
        q[iq].var[2]= uw;
        q[iq].var[3]= t;
        q[iq].var[4]= p;
        q[iq].var[5]= k;
        q[iq].var[6]= omega;
    }

    for( ipde=0;ipde<npde;ipde++ )
    {
        wrk[ipde]= new double[nq];
    }
    // read dof coordinates
    read_file(nx,nq,wrk,"./data/mesh1/xq/xq");
    transpose(nx,nq,wrk,xq);
    // read interface coordinates
    read_file(nx,nfc,xc,"./data/mesh1/xc/xc");
    // read frame speed
    read_file(1,nfc,wxdc,"./data/mesh1/wxdc/wxdc");
    // read face normals and area
    read_file(4,nfc,wnc,"./data/mesh1/wnc/wnc");
    // read connectivity arrays
    read_file(2,nfc,ifq,"./data/mesh1/ifq/ifq");
    // read pre-computed values for changes in the conserved variables
    // renumber and colour mesh using Reverse-Cuthill Mckee and
    // one of the two R. Lohner algorithms.
    renumber_grid();
    // undivided differenes and assemble metric tensor
    grad();
    // compute auxiliary variables
    fld->auxv(0, nq, q, aux);

    // cleanup buffer
    for( ipde=0;ipde<npde;ipde++ )
    {
        delete[] wrk[ipde];
    }
}

void cDomain::grad()
{
    int ix,jx,ic,ipde,iq,iql,iqr;
    int i;
    double eps=1.e-9;
    double d;
    double w;
    double *wrk[1];

    for( iq=0;iq<nq;iq++ )
    {
        dxdx[iq].var[0]= eps;
        dxdx[iq].var[1]= 0.;
        dxdx[iq].var[2]= 0.;
        dxdx[iq].var[3]= 0.;
        dxdx[iq].var[4]= eps;
        dxdx[iq].var[5]= 0.;
        dxdx[iq].var[6]= 0.;
        dxdx[iq].var[7]= 0.;
        dxdx[iq].var[8]= eps;
    }
    for( iq=0;iq<nq;iq++ )
    {
        for( ix=0;ix<nx;ix++ )
        {
            for( ipde=0;ipde<npde;ipde++ )
            {
                dqdx[iq].var[ix][ipde]= 0.;
            }
        }
    }

    wrk[0]= new double[nfc];
    setv(0,nfc, 1,0., wrk);

    for( ix=0;ix<nx;ix++ )
    {
        for( ic=0;ic<nfc;ic++ )
        {
            iql= ifq[0][ic];
            iqr= ifq[1][ic];
            d= xq[iqr].var[ix]- xq[iql].var[ix];
            d*= d;
            wrk[0][ic]+= d;
        }
    }

    for( ic=0;ic<nfc;ic++ )
    {
        wrk[0][ic]= 1./wrk[0][ic];
    }

    for( ix=0;ix<nx;ix++ )
    {
        for( ipde=0;ipde<npde;ipde++ )
        {
            for( ic=0;ic<nfc;ic++ )
            {
                iql= ifq[0][ic];
                iqr= ifq[1][ic];
                w= wrk[0][ic];
                d= w*(xq[iqr].var[ix]-xq[iql].var[ix])*(q[iqr].var[ipde]-q[iql].var[ipde]);
                dqdx[iql].var[ix][ipde]+= d;
                dqdx[iqr].var[ix][ipde]+= d;
            }
        }
        for( jx=0;jx<nx;jx++ )
        {
            for( ic=0;ic<nfc;ic++ )
            {
                iql= ifq[0][ic];
                iqr= ifq[1][ic];
                w= wrk[0][ic];
                d= w*(xq[iqr].var[ix]-xq[iql].var[ix])*(xq[iqr].var[jx]-xq[iql].var[jx]);
                dxdx[iql].var[ix*nx+jx]+= d;
                dxdx[iqr].var[ix*nx+jx]+= d;
            }
        }
    }
    delete[] wrk[0];
}

void cDomain::run_flux_kernel()
{
    int istart0, ifinish0, istart1, ifinish1;
    // get iteration spaces for vector and non-vector kernel calls
    //get_vec_loop_iter(0,nfc,&istart0,&ifinish0,&istart1,&ifinish1);
    // main vectorized version
    fld->iflux(0,nfc,ifq, xq, q, dxdx, dqdx, aux, rhs, xc, wnc, wxdc, auxf);
    // reference version for remainder
   // fld->iflux0(istart1,ifinish1,ifq, xq, q, dxdx, dqdx, aux, rhs, xc, wnc, wxdc, auxf);
}



void cDomain::output_results()
{
    // permute data structures to original layout
    // for verification with reference version
    permute(nq, npde, rhs, indx);
    write_text_file(npde, nq, rhs,  "./data/results/FLUX_PREFETCH.dat");
}

cDomain::~cDomain()
{
    // deallocate dof data structures
    _mm_free(q);      q=      NULL;
    _mm_free(aux);    aux=    NULL;
    _mm_free(daux);   daux=   NULL;
    _mm_free(rhs);    rhs=    NULL;
    _mm_free(dxdx);   dxdx=   NULL;
    _mm_free(dqdx);   dqdx=   NULL;
    // deallocate interface data structures
    _mm_free(swnc);   swnc=   NULL;
    _mm_free(sifq);   sifq=   NULL;
    _mm_free(sauxf);  sauxf=  NULL;
    _mm_free(sxc);    sxc=    NULL;
    _mm_free(swxdc);  swxdc=  NULL;
    // deallocate gas object
    delete fld;       fld=    NULL;
}
