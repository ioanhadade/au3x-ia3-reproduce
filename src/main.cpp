# include <iostream>
# include <domain.h>
# include <stopwatch.h>

using namespace std;

int main()
{
    cDomain *cfd;
    cStopwatch flux_sw("iflux kernel (face-based loop)");
    // create CFD domain
    cfd= new cDomain(3223052, 6675718, 3, 3, 7, 7, 7);
    // initialize and allocate data structures
    cfd->init(0, 0.2959, 0, 273.15, 10.1325, 0.0004, 444.44);
    cout << "Initialisation completed." << endl;
    // run example kernels
#ifdef ENABLE_PF
    cout << "Executing flux (face-based) kernel with software prefetching ACTIVATED." << endl;
#else
    cout << "Executing flux (face-based) kernel with software prefetching DEACTIVATED." << endl;
#endif
    flux_sw.reset();
    cfd->run_flux_kernel();
    flux_sw.record();
//    cfd->output_results();
    cout << flux_sw;
    delete cfd;
}
