# include <cmath>
# include <gas.h>

  cGas::cGas()
 {
      nx= 3;
      nvel= 3;
      npde= 7;
      unit[0]= 100.;
      unit[1]=   1.;
      unit[2]= unit[0]*unit[0];
      deflt[0]=   0.;
      deflt[1]= 298.;
      deflt[2]= 100000./unit[2];
      rg= 287/unit[2];
      gam= 1.4;
      eps= 0.05;
 };

  cGas::cGas( int nx, int nvel, int npde ):nx(nx),nvel(nvel),npde(npde)
 {
      unit[0]= 100.;
      unit[1]=   1.;
      unit[2]= unit[0]*unit[0];
      deflt[0]=   0.;
      deflt[1]= 298.;
      deflt[2]= 100000./unit[2];
      rg= 287/unit[2];
      gam= 1.4;
      eps= 0.05;
 };

 //compute mu using sutherland's law
   void cGas::auxv( int iqs, int iqe, aos1x8_t *q, aos1x8_t *aux )
  {
      int iq; 
      int ipde;
      double            mu=1.85e-5,kappa=2.624e-2,pr=0.7;
      double            l = 1.45e-6;
      double            c = 110.;

      kappa/= (unit[0]*unit[0]*unit[0]);
      double cp=rg*gam/(gam-1);
      
      for( iq=iqs;iq<iqe;iq++ )
     {
         //suhterland's law
         mu = l*pow(q[iq].var[3], 1.5)/(q[iq].var[3]+c);
         mu/= unit[0];

// density
         aux[iq].var[0]= q[iq].var[4]/( rg*q[iq].var[3] );
// kinetic energy
         aux[iq].var[1]=  q[iq].var[0]*q[iq].var[0];
         aux[iq].var[1]+= q[iq].var[1]*q[iq].var[1];
         aux[iq].var[1]+= q[iq].var[2]*q[iq].var[2];
         aux[iq].var[1]*= 0.5;
// speed of sound and total entalpy
         aux[iq].var[2]= gam*rg* q[iq].var[3]; 
         aux[iq].var[3]= aux[iq].var[2]/(gam-1)+ aux[iq].var[1];
         aux[iq].var[2]= sqrt( aux[iq].var[2] );
         aux[iq].var[4]= cp;
         aux[iq].var[5]= mu;
         aux[iq].var[6]= kappa;
     } 
  }
