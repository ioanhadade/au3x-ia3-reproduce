# include <cmath>
# include <sizes.h>
# include <gas.h>
# include <var.h>
# include <vec.h>


/** Computes second order MUSCL fluxes across a list of interfaces.
 *  Face-based loop example (vectorization).
    npde:            Number of unknowns
    ics:             Starting index in face list
    ice:             Ending index in face list
    ifq:             Pointers to left and right states. ifq[0][ic] is the left state of interface ic and ifq[1][ic] is right state of interface ic.
    xq:              Three coordinates of the interface.
    q0:              Flow variables of states.
    dxdx:            Metric tensors of left and right states.
    dqdx:            Undivided differences of left and right states.
    aux0:            Auxiliary flow variables of left and right states.
    rhs:             Accumulated right hand sides for left and right states.
    xc:              Position of the interface ic. xc[ix][ic] is the ix-th coordinate of interface ic.
    wc:              Scaled normal of the interface ic. wc[ix][ic] is the ix-th component of the normal direction to interface ic.
                     wc[3][ic] is the surface area of interface ic.
    wxdc:            integrated normal frame velocity to interface ic. wxdc[0][ic] is the integral of the frame speed on interface ic.
    auxc:            Auxiliary variables for interface ic. auxc[iv][ic] is the iv-th auxiliary variable for interface ic.
    deltq:           This is function is needed to perform the MUSCL extrapolation from the positions of the left and right state
                                                      onto the position of the interface.
 */

void cGas::iflux(int ics, int ice, int *ifq[], aos1x4_t *xq, aos1x8_t *q0, aos1x9_t *dxdx, aos3x8_t *dqdx, aos1x8_t *aux0,
                 aos1x8_t *rhs, double *xc[], double *wc[], double *wxdc[], double *auxc[])
{
    double            dqr[MAXNPDES][VECLEN]                 __attribute__((aligned(ALIGN)));
    double            dql[MAXNPDES][VECLEN]                 __attribute__((aligned(ALIGN)));
    double            xn[3][VECLEN]                         __attribute__((aligned(ALIGN)));
    double            wn[4][VECLEN]                         __attribute__((aligned(ALIGN)));
    double            fl[MAXNPDES]                          __attribute__((aligned(ALIGN)));
    double            fr[MAXNPDES]                          __attribute__((aligned(ALIGN)));
    double            ql[MAXNPDES]                          __attribute__((aligned(ALIGN)));
    double            qr[MAXNPDES]                          __attribute__((aligned(ALIGN)));
    double            qa[MAXNPDES]                          __attribute__((aligned(ALIGN)));
    double            rhsl[MAXNPDES][VECLEN]                __attribute__((aligned(ALIGN)));
    double            rhsr[MAXNPDES][VECLEN]                __attribute__((aligned(ALIGN)));
    double            fa[MAXNPDES]                          __attribute__((aligned(ALIGN)));
    double            dq[MAXNPDES][VECLEN]                  __attribute__((aligned(ALIGN)));
    double            f[MAXNPDES][VECLEN]                   __attribute__((aligned(ALIGN)));
    double            ql0[MAXNPDES][VECLEN]                 __attribute__((aligned(ALIGN)));
    double            qr0[MAXNPDES][VECLEN]                 __attribute__((aligned(ALIGN)));
    double            xql[3][VECLEN]                        __attribute__((aligned(ALIGN)));
    double            xqr[3][VECLEN]                        __attribute__((aligned(ALIGN)));
    double            dxdxl[9][VECLEN]                      __attribute__((aligned(ALIGN)));
    double            dxdxr[9][VECLEN]                      __attribute__((aligned(ALIGN)));
    double            dqdxl[MAXNPDES][3][VECLEN]            __attribute__((aligned(ALIGN)));
    double            dqdxr[MAXNPDES][3][VECLEN]            __attribute__((aligned(ALIGN)));
    double            fs[VECLEN]                            __attribute__((aligned(ALIGN)));
    double            rl,ll1,ll3,ll4,pl,hl,al,unl,kel;
    double            rr,lr1,lr3,lr4,pr,hr,ar,unr,ker;
    double            le3,le4;
    double            aa,a2a,ra,ha,ka,ana[3];
    double            la1,la4,la3,una,unaa,raa,lmax;
    double            dw1,dw3,dw4,dw2[3],dw2a;
    double            dr,dun,dp,dpa,dw5[2],wl,wr;
    int               iv,ix,ic,ipde;
    double            gam1=  gam-1.;
    double            gam1r= 1./gam1;
    const double cp=  rg*gam/(gam1);


    for( ic=ics;ic<ice;ic+=VECLEN )
    {

        // 1. Load/gather variables into short vectors
        // allocated on the stack and aligned at correct
        // byte boundaries.

        // the addition of the non-portable vector aligned clause
        // is necessary since the Intel compilers do not seem to enforce
        // the aligned clause for omp simd
        // issue prefetches for the left and right indices
        prefetchi(&ifq[0][ic],&ifq[1][ic]);

# if defined __INTEL_COMPILER
        #pragma vector aligned
# endif
# if defined _OPENMP && _OPENMP >= 201307
        #pragma omp simd simdlen(VECLEN) safelen(VECLEN)
# endif
        for( iv=0;iv<VECLEN;iv++ )
        {
            wn[0][iv]= wc[0][ic+iv];
            wn[1][iv]= wc[1][ic+iv];
            wn[2][iv]= wc[2][ic+iv];
            wn[3][iv]= wc[3][ic+iv];
            xn[0][iv]= xc[0][ic+iv];
            xn[1][iv]= xc[1][ic+iv];
            xn[2][iv]= xc[2][ic+iv];
            fs[iv]   = wxdc[0][ic+iv];
        }

        vload(wc,4,ic,wn);
        vload(xc,3,ic,xn);
        vload(wxdc,ic,fs);

        // gather cell-centered variables
        // we are manually unrolling here
        // where the size of the inner loops are known
        // left and right states
        vgather(q0, 7, &ifq[0][ic], ql0);
        vgather(q0, 7, &ifq[1][ic], qr0);

        // left and right undivided differences
        vgather(dqdx, 3, 7, &ifq[0][ic], dqdxl);
        vgather(dqdx, 3, 7, &ifq[1][ic], dqdxr);

        // left and right cell-center coordinates
        vgather(xq, 3, &ifq[0][ic], xql);
        vgather(xq, 3, &ifq[1][ic], xqr);

        // metric tensor of left and right states
        vgather(dxdx, 9, &ifq[0][ic], dxdxl);
        vgather(dxdx, 9, &ifq[1][ic], dxdxr);

        // residuals for accumulation
        vgather(rhs, 7, &ifq[0][ic], rhsl);
        vgather(rhs, 7, &ifq[1][ic], rhsr);

        // 2. Perform computation
        // gradient extrapolation -- should be inlined, see domain.h
        deltq(xql,ql0,dxdxl,dqdxl,xqr,qr0,dxdxr,dqdxr,xn,wn,dql,dqr);
        // prefetch Roe averages explicitly as we dont use vload
        prefetchd(auxc,7,ic);
# if defined __INTEL_COMPILER
        #pragma vector aligned
# endif
# if defined _OPENMP && _OPENMP >= 201307
        #pragma omp simd simdlen(VECLEN) safelen(VECLEN)
# endif
        for( iv=0;iv<VECLEN;iv++ )
        {
            ql[0]= ql0[0][iv]+dql[0][iv];
            ql[1]= ql0[1][iv]+dql[1][iv];
            ql[2]= ql0[2][iv]+dql[2][iv];
            ql[3]= ql0[3][iv]+dql[3][iv];
            ql[4]= ql0[4][iv]+dql[4][iv];
            ql[5]= ql0[5][iv]+dql[5][iv];
            ql[6]= ql0[6][iv]+dql[6][iv];

            qr[0]= qr0[0][iv]+dqr[0][iv];
            qr[1]= qr0[1][iv]+dqr[1][iv];
            qr[2]= qr0[2][iv]+dqr[2][iv];
            qr[3]= qr0[3][iv]+dqr[3][iv];
            qr[4]= qr0[4][iv]+dqr[4][iv];
            qr[5]= qr0[5][iv]+dqr[5][iv];
            qr[6]= qr0[6][iv]+dqr[6][iv];

            // left state - auxiliary variables
            pl= ql[4];
            rl= ql[4]/(rg*ql[3]);
            // kinetic energy
            kel= ((ql[0]*ql[0])+(ql[1]*ql[1])+(ql[2]*ql[2]))*0.5;
            // speed of sound
            al= gam*rg*ql[3];
            // enthalpy
            hl=al*gam1r+kel;
            al=sqrt(al);

            unl=  wn[0][iv]*ql[0];
            unl+= wn[1][iv]*ql[1];
            unl+= wn[2][iv]*ql[2];

            ll1= unl-fs[iv];
            ll3= ll1+ al;
            ll4= ll1- al;
            // fluxes from the left
            fl[0]= ll1*rl;
            fl[1]= fl[0]*ql[0]+   wn[0][iv]*pl;
            fl[2]= fl[0]*ql[1]+   wn[1][iv]*pl;
            fl[3]= fl[0]*ql[2]+   wn[2][iv]*pl;
            fl[4]= fl[0]*hl+    fs[iv]*pl;
            fl[5]= fl[0]*ql[5];
            fl[6]= fl[0]*ql[6];

            pr= qr[4];
            rr= qr[4]/(rg*qr[3]);
            // kinetic energy
            ker= ((qr[0]*qr[0])+(qr[1]*qr[1])+(qr[2]*qr[2]))*0.5;
            // speed of sound
            ar= gam*rg*qr[3];
            // enthalpy
            hr=ar*gam1r+ker;
            ar=sqrt(ar);

            unr=  wn[0][iv]*qr[0];
            unr+= wn[1][iv]*qr[1];
            unr+= wn[2][iv]*qr[2];

            lr1= unr-fs[iv];
            lr3= lr1+ ar;
            lr4= lr1- ar;
            // fluxes from the right
            fr[0]= lr1 *rr;
            fr[1]= fr[0]*qr[0]+ wn[0][iv]*pr;
            fr[2]= fr[0]*qr[1]+ wn[1][iv]*pr;
            fr[3]= fr[0]*qr[2]+ wn[2][iv]*pr;
            fr[4]= fr[0]*hr+    fs[iv]*pr;
            fr[5]= fr[0]*qr[5];
            fr[6]= fr[0]*qr[6];

            // Roe averages
            ra= rr/rl;
            ra= sqrt(ra);
            wl= 1.+ra;
            wl= 1./wl;
            wr= 1.-wl;

            ra*= rl;

            qa[0]= wl*ql[0]+ wr*qr[0];
            qa[1]= wl*ql[1]+ wr*qr[1];
            qa[2]= wl*ql[2]+ wr*qr[2];
            qa[3]= wl*ql[3]+ wr*qr[3];
            qa[4]= wl*ql[4]+ wr*qr[4];
            qa[5]= wl*ql[5]+ wr*qr[5];
            qa[6]= wl*ql[6]+ wr*qr[6];


            ka=  qa[0]*qa[0];
            ka+= qa[1]*qa[1];
            ka+= qa[2]*qa[2];
            una=  qa[0]* wn[0][iv];
            una+= qa[1]* wn[1][iv];
            una+= qa[2]* wn[2][iv];
            ha= wl*hl+ wr*hr;
            ka*= 0.5;
            a2a= gam1*(ha- ka);
            aa= sqrt(a2a);
            raa=ra*aa;

            // eigenvalues with Harten's fix
            la1= una-fs[iv];
            la3= la1+ aa;
            la4= la1- aa;

            lmax= fabs(la1)+ aa;
            lmax= fmax(lmax, fabs(ll1)+ al);
            lmax= fmax(lmax, fabs(lr1)+ ar);

            le3= fmax(fmax(eps*lmax, la3-ll3), lr3-la3);
            le4= fmax(fmax(eps*lmax, la4-ll4), lr4-la4);

            la1= fabs(la1);
            la3= fabs(la3);
            la4= fabs(la4);
            if(la3 < le3)
            {
                la3= 0.5*(la3*la3/le3+ le3);
            }
            if(la4 < le4)
            {
                la4= 0.5*(la4*la4/le4+ le4);
            }

            // store Roe averages
            auxc[0][ic+iv]= wl;
            auxc[1][ic+iv]= wr;
            auxc[2][ic+iv]= ra;
            auxc[3][ic+iv]= la1;
            auxc[4][ic+iv]= la3;
            auxc[5][ic+iv]= la4;
            auxc[6][ic+iv]= lmax*wn[3][iv];

            // Left eigenvectors
            dr= rr- rl;
            dq[0][iv]= qr[0]- ql[0];
            dq[1][iv]= qr[1]- ql[1];
            dq[2][iv]= qr[2]- ql[2];
            dq[3][iv]= qr[3]- ql[3];
            dq[4][iv]= qr[4]- ql[4];
            dq[5][iv]= qr[5]- ql[5];
            dq[6][iv]= qr[6]- ql[6];

            dun= unr- unl;

            la3*=  0.5*ra/aa;
            la4*= -0.5*ra/aa;

            dp= pr-pl;
            dpa=dp/raa;
            dw1= la1*(dr-dp/a2a);
            dw3= la3*(dun+dpa);
            dw4= la4*(dun-dpa);
            dw2[0]= dq[0][iv]- dun*wn[0][iv]; dw2[0]*= ra*la1;
            dw2[1]= dq[1][iv]- dun*wn[1][iv]; dw2[1]*= ra*la1;
            dw2[2]= dq[2][iv]- dun*wn[2][iv]; dw2[2]*= ra*la1;
            dw2a=  dw2[0]*qa[0];
            dw2a+= dw2[1]*qa[1];
            dw2a+= dw2[2]*qa[2];
            ana[0]= aa*wn[0][iv];
            ana[1]= aa*wn[1][iv];
            ana[2]= aa*wn[2][iv];


            dw5[0]=dq[5][iv]*ra*la1;
            dw5[1]=dq[6][iv]*ra*la1;

            unaa=aa*una;

            // Roe fluxes
            fa[0]= dw1+               dw3+                  dw4;
            fa[1]= dw1*qa[0]+ dw2[0]+ dw3*(qa[0]+ ana[0])+ dw4*(qa[0]- ana[0]);
            fa[2]= dw1*qa[1]+ dw2[1]+ dw3*(qa[1]+ ana[1])+ dw4*(qa[1]- ana[1]);
            fa[3]= dw1*qa[2]+ dw2[2]+ dw3*(qa[2]+ ana[2])+ dw4*(qa[2]- ana[2]);
            fa[4]= dw1*ka +    dw2a+   dw3*(ha+ unaa)+     dw4*(ha- unaa);
            fa[5]= fa[0]*qa[5] + dw5[0];
            fa[6]= fa[0]*qa[6] + dw5[1];

            // assemble
            f[0][iv]= 0.5*(fr[0]+ fl[0]- fa[0])*wn[3][iv];
            f[1][iv]= 0.5*(fr[1]+ fl[1]- fa[1])*wn[3][iv];
            f[2][iv]= 0.5*(fr[2]+ fl[2]- fa[2])*wn[3][iv];
            f[3][iv]= 0.5*(fr[3]+ fl[3]- fa[3])*wn[3][iv];
            f[4][iv]= 0.5*(fr[4]+ fl[4]- fa[4])*wn[3][iv];
            f[5][iv]= 0.5*(fr[5]+ fl[5]- fa[5])*wn[3][iv];
            f[6][iv]= 0.5*(fr[6]+ fl[6]- fa[6])*wn[3][iv];
            // accumulate
            rhsl[0][iv]-= f[0][iv];
            rhsl[1][iv]-= f[1][iv];
            rhsl[2][iv]-= f[2][iv];
            rhsl[3][iv]-= f[3][iv];
            rhsl[4][iv]-= f[4][iv];
            rhsl[5][iv]-= f[5][iv];
            rhsl[6][iv]-= f[6][iv];

            rhsr[0][iv]+= f[0][iv];
            rhsr[1][iv]+= f[1][iv];
            rhsr[2][iv]+= f[2][iv];
            rhsr[3][iv]+= f[3][iv];
            rhsr[4][iv]+= f[4][iv];
            rhsr[5][iv]+= f[5][iv];
            rhsr[6][iv]+= f[6][iv];

        }
        // 3. Sscatter back residuals using vector scatter
        //    operations since faces are now coloured.
        vscatter(rhsl, 7, &ifq[0][ic], rhs);
        vscatter(rhsr, 7, &ifq[1][ic], rhs);
    }
}

void cGas::deltq(double xql[][VECLEN], double ql[][VECLEN], double dxdxl[][VECLEN], double dqdxl[][3][VECLEN],
                 double xqr[][VECLEN], double qr[][VECLEN], double dxdxr[][VECLEN], double dqdxr[][3][VECLEN],
                 double xn[3][VECLEN], double wn[][VECLEN], double dql[][VECLEN], double dqr[][VECLEN])
{
    int ix,jx,iv;
    double dx[VECLEN]               __attribute__((aligned(ALIGN)));
    double dy[VECLEN]               __attribute__((aligned(ALIGN)));
    double dz[VECLEN]               __attribute__((aligned(ALIGN)));
    double dqi[MAXNPDES][VECLEN]    __attribute__((aligned(ALIGN)));
    double wl[VECLEN]               __attribute__((aligned(ALIGN)));
    double wr[VECLEN]               __attribute__((aligned(ALIGN)));
    double wi[VECLEN]               __attribute__((aligned(ALIGN)));
    double w[VECLEN]                __attribute__((aligned(ALIGN)));
    const double eps= 1.e-16;

# if defined __INTEL_COMPILER
    #pragma vector aligned
# endif
# if defined _OPENMP && _OPENMP >= 201307
    #pragma omp simd simdlen(VECLEN) safelen(VECLEN)
# endif
    for( iv=0;iv<VECLEN;iv++ )
    {
        dx[iv]= xqr[0][iv]- xql[0][iv];
        dy[iv]= xqr[1][iv]- xql[1][iv];
        dz[iv]= xqr[2][iv]- xql[2][iv];
        w[iv]= (dx[iv]*dx[iv])+(dy[iv]*dy[iv])+(dz[iv]*dz[iv]);
        wi[iv]= (dx[iv]*wn[0][iv])+(dy[iv]*wn[1][iv])+(dz[iv]*wn[2][iv]);

        w[iv]= 1./w[iv];       // reciprocal
        wi[iv]= 1./wi[iv];     // reciprocal

        dqi[0][iv]= (qr[0][iv]- ql[0][iv]);
        dqi[1][iv]= (qr[1][iv]- ql[1][iv]);
        dqi[2][iv]= (qr[2][iv]- ql[2][iv]);
        dqi[3][iv]= (qr[3][iv]- ql[3][iv]);
        dqi[4][iv]= (qr[4][iv]- ql[4][iv]);
        dqi[5][iv]= (qr[5][iv]- ql[5][iv]);
        dqi[6][iv]= (qr[6][iv]- ql[6][iv]);

        dxdxl[0][iv]-= w[iv]*dx[iv]*dx[iv];
        dxdxl[1][iv]-= w[iv]*dx[iv]*dy[iv];
        dxdxl[2][iv]-= w[iv]*dx[iv]*dz[iv];
        dxdxl[3][iv]-= w[iv]*dy[iv]*dx[iv];
        dxdxl[4][iv]-= w[iv]*dy[iv]*dy[iv];
        dxdxl[5][iv]-= w[iv]*dy[iv]*dz[iv];
        dxdxl[6][iv]-= w[iv]*dz[iv]*dx[iv];
        dxdxl[7][iv]-= w[iv]*dz[iv]*dy[iv];
        dxdxl[8][iv]-= w[iv]*dz[iv]*dz[iv];

        dxdxr[0][iv]-= w[iv]*dx[iv]*dx[iv];
        dxdxr[1][iv]-= w[iv]*dx[iv]*dy[iv];
        dxdxr[2][iv]-= w[iv]*dx[iv]*dz[iv];
        dxdxr[3][iv]-= w[iv]*dy[iv]*dx[iv];
        dxdxr[4][iv]-= w[iv]*dy[iv]*dy[iv];
        dxdxr[5][iv]-= w[iv]*dy[iv]*dz[iv];
        dxdxr[6][iv]-= w[iv]*dz[iv]*dx[iv];
        dxdxr[7][iv]-= w[iv]*dz[iv]*dy[iv];
        dxdxr[8][iv]-= w[iv]*dz[iv]*dz[iv];

        dqdxl[0][0][iv]-=w[iv]*dx[iv]*dqi[0][iv];
        dqdxl[0][1][iv]-=w[iv]*dy[iv]*dqi[0][iv];
        dqdxl[0][2][iv]-=w[iv]*dz[iv]*dqi[0][iv];

        dqdxl[1][0][iv]-=w[iv]*dx[iv]*dqi[1][iv];
        dqdxl[1][1][iv]-=w[iv]*dy[iv]*dqi[1][iv];
        dqdxl[1][2][iv]-=w[iv]*dz[iv]*dqi[1][iv];

        dqdxl[2][0][iv]-=w[iv]*dx[iv]*dqi[2][iv];
        dqdxl[2][1][iv]-=w[iv]*dy[iv]*dqi[2][iv];
        dqdxl[2][2][iv]-=w[iv]*dz[iv]*dqi[2][iv];

        dqdxl[3][0][iv]-=w[iv]*dx[iv]*dqi[3][iv];
        dqdxl[3][1][iv]-=w[iv]*dy[iv]*dqi[3][iv];
        dqdxl[3][2][iv]-=w[iv]*dz[iv]*dqi[3][iv];

        dqdxl[4][0][iv]-=w[iv]*dx[iv]*dqi[4][iv];
        dqdxl[4][1][iv]-=w[iv]*dy[iv]*dqi[4][iv];
        dqdxl[4][2][iv]-=w[iv]*dz[iv]*dqi[4][iv];

        dqdxl[5][0][iv]-=w[iv]*dx[iv]*dqi[5][iv];
        dqdxl[5][1][iv]-=w[iv]*dy[iv]*dqi[5][iv];
        dqdxl[5][2][iv]-=w[iv]*dz[iv]*dqi[5][iv];

        dqdxl[6][0][iv]-=w[iv]*dx[iv]*dqi[6][iv];
        dqdxl[6][1][iv]-=w[iv]*dy[iv]*dqi[6][iv];
        dqdxl[6][2][iv]-=w[iv]*dz[iv]*dqi[6][iv];

        dqdxr[0][0][iv]-=w[iv]*dx[iv]*dqi[0][iv];
        dqdxr[0][1][iv]-=w[iv]*dy[iv]*dqi[0][iv];
        dqdxr[0][2][iv]-=w[iv]*dz[iv]*dqi[0][iv];

        dqdxr[1][0][iv]-=w[iv]*dx[iv]*dqi[1][iv];
        dqdxr[1][1][iv]-=w[iv]*dy[iv]*dqi[1][iv];
        dqdxr[1][2][iv]-=w[iv]*dz[iv]*dqi[1][iv];

        dqdxr[2][0][iv]-=w[iv]*dx[iv]*dqi[2][iv];
        dqdxr[2][1][iv]-=w[iv]*dy[iv]*dqi[2][iv];
        dqdxr[2][2][iv]-=w[iv]*dz[iv]*dqi[2][iv];

        dqdxr[3][0][iv]-=w[iv]*dx[iv]*dqi[3][iv];
        dqdxr[3][1][iv]-=w[iv]*dy[iv]*dqi[3][iv];
        dqdxr[3][2][iv]-=w[iv]*dz[iv]*dqi[3][iv];

        dqdxr[4][0][iv]-=w[iv]*dx[iv]*dqi[4][iv];
        dqdxr[4][1][iv]-=w[iv]*dy[iv]*dqi[4][iv];
        dqdxr[4][2][iv]-=w[iv]*dz[iv]*dqi[4][iv];

        dqdxr[5][0][iv]-=w[iv]*dx[iv]*dqi[5][iv];
        dqdxr[5][1][iv]-=w[iv]*dy[iv]*dqi[5][iv];
        dqdxr[5][2][iv]-=w[iv]*dz[iv]*dqi[5][iv];

        dqdxr[6][0][iv]-=w[iv]*dx[iv]*dqi[6][iv];
        dqdxr[6][1][iv]-=w[iv]*dy[iv]*dqi[6][iv];
        dqdxr[6][2][iv]-=w[iv]*dz[iv]*dqi[6][iv];

        // mininum norm
        wl[iv]= (dxdxl[0][iv]*wn[0][iv]*wn[0][iv])+(dxdxl[1][iv]*wn[0][iv]*wn[1][iv])+(dxdxl[2][iv]*wn[0][iv]*wn[2][iv]) +
                (dxdxl[3][iv]*wn[1][iv]*wn[0][iv])+(dxdxl[4][iv]*wn[1][iv]*wn[1][iv])+(dxdxl[5][iv]*wn[1][iv]*wn[2][iv]) +
                (dxdxl[6][iv]*wn[2][iv]*wn[0][iv])+(dxdxl[7][iv]*wn[2][iv]*wn[1][iv])+(dxdxl[8][iv]*wn[2][iv]*wn[2][iv]);

        wr[iv]= (dxdxr[0][iv]*wn[0][iv]*wn[0][iv])+(dxdxr[1][iv]*wn[0][iv]*wn[1][iv])+(dxdxr[2][iv]*wn[0][iv]*wn[2][iv]) +
                (dxdxr[3][iv]*wn[1][iv]*wn[0][iv])+(dxdxr[4][iv]*wn[1][iv]*wn[1][iv])+(dxdxr[5][iv]*wn[1][iv]*wn[2][iv]) +
                (dxdxr[6][iv]*wn[2][iv]*wn[0][iv])+(dxdxr[7][iv]*wn[2][iv]*wn[1][iv])+(dxdxr[8][iv]*wn[2][iv]*wn[2][iv]);

        wl[iv]=1./wl[iv];
        wr[iv]=1./wr[iv];

        dql[0][iv] = ((dqdxl[0][0][iv]*wn[0][iv])+(dqdxl[0][1][iv]*wn[1][iv])+(dqdxl[0][2][iv]*wn[2][iv]))*wl[iv];
        dql[1][iv] = ((dqdxl[1][0][iv]*wn[0][iv])+(dqdxl[1][1][iv]*wn[1][iv])+(dqdxl[1][2][iv]*wn[2][iv]))*wl[iv];
        dql[2][iv] = ((dqdxl[2][0][iv]*wn[0][iv])+(dqdxl[2][1][iv]*wn[1][iv])+(dqdxl[2][2][iv]*wn[2][iv]))*wl[iv];
        dql[3][iv] = ((dqdxl[3][0][iv]*wn[0][iv])+(dqdxl[3][1][iv]*wn[1][iv])+(dqdxl[3][2][iv]*wn[2][iv]))*wl[iv];
        dql[4][iv] = ((dqdxl[4][0][iv]*wn[0][iv])+(dqdxl[4][1][iv]*wn[1][iv])+(dqdxl[4][2][iv]*wn[2][iv]))*wl[iv];
        dql[5][iv] = ((dqdxl[5][0][iv]*wn[0][iv])+(dqdxl[5][1][iv]*wn[1][iv])+(dqdxl[5][2][iv]*wn[2][iv]))*wl[iv];
        dql[6][iv] = ((dqdxl[6][0][iv]*wn[0][iv])+(dqdxl[6][1][iv]*wn[1][iv])+(dqdxl[6][2][iv]*wn[2][iv]))*wl[iv];

        dqr[0][iv] = ((dqdxr[0][0][iv]*wn[0][iv])+(dqdxr[0][1][iv]*wn[1][iv])+(dqdxr[0][2][iv]*wn[2][iv]))*wr[iv];
        dqr[1][iv] = ((dqdxr[1][0][iv]*wn[0][iv])+(dqdxr[1][1][iv]*wn[1][iv])+(dqdxr[1][2][iv]*wn[2][iv]))*wr[iv];
        dqr[2][iv] = ((dqdxr[2][0][iv]*wn[0][iv])+(dqdxr[2][1][iv]*wn[1][iv])+(dqdxr[2][2][iv]*wn[2][iv]))*wr[iv];
        dqr[3][iv] = ((dqdxr[3][0][iv]*wn[0][iv])+(dqdxr[3][1][iv]*wn[1][iv])+(dqdxr[3][2][iv]*wn[2][iv]))*wr[iv];
        dqr[4][iv] = ((dqdxr[4][0][iv]*wn[0][iv])+(dqdxr[4][1][iv]*wn[1][iv])+(dqdxr[4][2][iv]*wn[2][iv]))*wr[iv];
        dqr[5][iv] = ((dqdxr[5][0][iv]*wn[0][iv])+(dqdxr[5][1][iv]*wn[1][iv])+(dqdxr[5][2][iv]*wn[2][iv]))*wr[iv];
        dqr[6][iv] = ((dqdxr[6][0][iv]*wn[0][iv])+(dqdxr[6][1][iv]*wn[1][iv])+(dqdxr[6][2][iv]*wn[2][iv]))*wr[iv];

        dqi[0][iv]*=wi[iv];
        dqi[1][iv]*=wi[iv];
        dqi[2][iv]*=wi[iv];
        dqi[3][iv]*=wi[iv];
        dqi[4][iv]*=wi[iv];
        dqi[5][iv]*=wi[iv];
        dqi[6][iv]*=wi[iv];


        // valbada limiter -- manually inlined
        ((dql[0][iv]*dqi[0][iv])<=0) ? dql[0][iv]=0. : dql[0][iv]= (dqi[0][iv]*(dql[0][iv]*dql[0][iv]+eps)+dql[0][iv]*
                (dqi[0][iv]*dqi[0][iv]+eps))/
                (dql[0][iv]*dql[0][iv]+dqi[0][iv]*dqi[0][iv]+2*eps);

        ((dql[1][iv]*dqi[1][iv])<=0) ? dql[1][iv]=0. : dql[1][iv]= (dqi[1][iv]*(dql[1][iv]*dql[1][iv]+eps)+dql[1][iv]*
                (dqi[1][iv]*dqi[1][iv]+eps))/
                (dql[1][iv]*dql[1][iv]+dqi[1][iv]*dqi[1][iv]+2*eps);

        ((dql[2][iv]*dqi[2][iv])<=0) ? dql[2][iv]=0. : dql[2][iv]= (dqi[2][iv]*(dql[2][iv]*dql[2][iv]+eps)+dql[2][iv]*
                (dqi[2][iv]*dqi[2][iv]+eps))/
                (dql[2][iv]*dql[2][iv]+dqi[2][iv]*dqi[2][iv]+2*eps);

        ((dql[3][iv]*dqi[3][iv])<=0) ? dql[3][iv]=0. : dql[3][iv]= (dqi[3][iv]*(dql[3][iv]*dql[3][iv]+eps)+dql[3][iv]*
                (dqi[3][iv]*dqi[3][iv]+eps))/
                (dql[3][iv]*dql[3][iv]+dqi[3][iv]*dqi[3][iv]+2*eps);

        ((dql[4][iv]*dqi[4][iv])<=0) ? dql[4][iv]=0. : dql[4][iv]= (dqi[4][iv]*(dql[4][iv]*dql[4][iv]+eps)+dql[4][iv]*
                (dqi[4][iv]*dqi[4][iv]+eps))/
                (dql[4][iv]*dql[4][iv]+dqi[4][iv]*dqi[4][iv]+2*eps);

        ((dql[5][iv]*dqi[5][iv])<=0) ? dql[5][iv]=0. : dql[5][iv]= (dqi[5][iv]*(dql[5][iv]*dql[5][iv]+eps)+dql[5][iv]*
                (dqi[5][iv]*dqi[5][iv]+eps))/
                (dql[5][iv]*dql[5][iv]+dqi[5][iv]*dqi[5][iv]+2*eps);

        ((dql[6][iv]*dqi[6][iv])<=0) ? dql[6][iv]=0. : dql[6][iv]= (dqi[6][iv]*(dql[6][iv]*dql[6][iv]+eps)+dql[6][iv]*
                (dqi[6][iv]*dqi[6][iv]+eps))/
                (dql[6][iv]*dql[6][iv]+dqi[6][iv]*dqi[6][iv]+2*eps);
        // valbada limiter -- manually inlined
        ((dqr[0][iv]*dqi[0][iv])<=0) ? dqr[0][iv]=0. : dqr[0][iv]= (dqi[0][iv]*(dqr[0][iv]*dqr[0][iv]+eps)+dqr[0][iv]*
                (dqi[0][iv]*dqi[0][iv]+eps))/
                (dqr[0][iv]*dqr[0][iv]+dqi[0][iv]*dqi[0][iv]+2*eps);
        ((dqr[1][iv]*dqi[1][iv])<=0) ? dqr[1][iv]=0. : dqr[1][iv]= (dqi[1][iv]*(dqr[1][iv]*dqr[1][iv]+eps)+dqr[1][iv]*
                (dqi[1][iv]*dqi[1][iv]+eps))/
                (dqr[1][iv]*dqr[1][iv]+dqi[1][iv]*dqi[1][iv]+2*eps);
        ((dqr[2][iv]*dqi[2][iv])<=0) ? dqr[2][iv]=0. : dqr[2][iv]= (dqi[2][iv]*(dqr[2][iv]*dqr[2][iv]+eps)+dqr[2][iv]*
                (dqi[2][iv]*dqi[2][iv]+eps))/
                (dqr[2][iv]*dqr[2][iv]+dqi[2][iv]*dqi[2][iv]+2*eps);
        ((dqr[3][iv]*dqi[3][iv])<=0) ? dqr[3][iv]=0. : dqr[3][iv]= (dqi[3][iv]*(dqr[3][iv]*dqr[3][iv]+eps)+dqr[3][iv]*
                (dqi[3][iv]*dqi[3][iv]+eps))/
                (dqr[3][iv]*dqr[3][iv]+dqi[3][iv]*dqi[3][iv]+2*eps);
        ((dqr[4][iv]*dqi[4][iv])<=0) ? dqr[4][iv]=0. : dqr[4][iv]= (dqi[4][iv]*(dqr[4][iv]*dqr[4][iv]+eps)+dqr[4][iv]*
                (dqi[4][iv]*dqi[4][iv]+eps))/
                (dqr[4][iv]*dqr[4][iv]+dqi[4][iv]*dqi[4][iv]+2*eps);
        ((dqr[5][iv]*dqi[5][iv])<=0) ? dqr[5][iv]=0. : dqr[5][iv]= (dqi[5][iv]*(dqr[5][iv]*dqr[5][iv]+eps)+dqr[5][iv]*
                (dqi[5][iv]*dqi[5][iv]+eps))/
                (dqr[5][iv]*dqr[5][iv]+dqi[5][iv]*dqi[5][iv]+2*eps);
        ((dqr[6][iv]*dqi[6][iv])<=0) ? dqr[6][iv]=0. : dqr[6][iv]= (dqi[6][iv]*(dqr[6][iv]*dqr[6][iv]+eps)+dqr[6][iv]*
                (dqi[6][iv]*dqi[6][iv]+eps))/
                (dqr[6][iv]*dqr[6][iv]+dqi[6][iv]*dqi[6][iv]+2*eps);
        wl[iv]= (wn[0][iv]*(xn[0][iv]-xql[0][iv]))+(wn[1][iv]*(xn[1][iv]-xql[1][iv]))+(wn[2][iv]*(xn[2][iv]-xql[2][iv]));
        wr[iv]= (wn[0][iv]*(xqr[0][iv]-xn[0][iv]))+(wn[1][iv]*(xqr[1][iv]-xn[1][iv]))+(wn[2][iv]*(xqr[2][iv]-xn[2][iv]));


        dql[0][iv]*=(wl[iv]);
        dql[1][iv]*=(wl[iv]);
        dql[2][iv]*=(wl[iv]);
        dql[3][iv]*=(wl[iv]);
        dql[4][iv]*=(wl[iv]);
        dql[5][iv]*=(wl[iv]);
        dql[6][iv]*=(wl[iv]);

        dqr[0][iv]*=(-wr[iv]);
        dqr[1][iv]*=(-wr[iv]);
        dqr[2][iv]*=(-wr[iv]);
        dqr[3][iv]*=(-wr[iv]);
        dqr[4][iv]*=(-wr[iv]);
        dqr[5][iv]*=(-wr[iv]);
        dqr[6][iv]*=(-wr[iv]);
    }
}


void valbada(int npde, double *r1, double *r2, double eps, double *val)
{
    int ipde;
    double r;
    for( ipde=0;ipde<npde;ipde++ )
    {
        r= r1[ipde]*r2[ipde];
        if(r <= 0)
        {
            val[ipde]= 0;
        }
        else
        {
            val[ipde]= (r2[ipde]*(r1[ipde]*r1[ipde]+eps)+r1[ipde]*(r2[ipde]*r2[ipde]+ eps))/
                       (r1[ipde]*r1[ipde]+ r2[ipde]*r2[ipde]+ 2*eps);
        }
    }
}
