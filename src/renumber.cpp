# include <cassert>
# include <sizes.h>
# include <utils.h>
# include <domain.h>


void     rcmk(int, int *[], int **, int *, int *, int *);
void     build_graph(int, int nedge, int *[], int *lg[], int **ig);

void cDomain::renumber_grid()
{
    int       *lg[2];
    int       **ig;
    int       *iwrk;
    int       *fprm;
    int       i,j,k,h,m,ipde,ix,ivar;
    int       tmp[MAXNEIGH];
    int       *jmp;
    int       i0,i1;

    // alocate storage
    lg[0]= new int[nq]; setv(0,nq, 0,lg[0]);
    lg[1]= new int[nq]; setv(0,nq, 0,lg[1]);
    ig= new int *[nq]; setv(0,nq, (int *)NULL, ig);
    iwrk= new int[nq];
    iprm= new int[nq];
    indx= new int[nq];
    fprm= new int[nfc];
    jmp= new int[nq]; setv(0,nq,0,jmp);


    // build graph using connectivity array
    build_graph(nq,nfc,ifq,lg,ig);

    // Reverse Cuthill-Mckee
    rcmk(nq,lg,ig,iprm,indx,iwrk);

    // relabel faces
    relabel(nfc, ifq[0], indx);
    relabel(nfc, ifq[1], indx);

    // sort faces according to first index
    hsort(nfc,ifq[0],fprm);
    // perform permutations
    permute(nfc,ifq[0],fprm);
    permute(nfc,ifq[1],fprm);

    // permute all previously existing data structures
    permute(nq,npde,q,iprm);
    permute(nq,nx,xq,iprm);

    for( ivar=0;ivar<4;ivar++ )
    {
        permute(nfc,wnc[ivar],fprm);
    }

    for( ix=0;ix<nx;ix++ )
    {
        permute(nfc,xc[ix],fprm);
    }

    permute(nfc,wxdc[0],fprm);

    // sort right hand side indices in ascending order
    // could be done way more elegantly than this.
    for( i=0;i<nfc;i++ )
    {
        j=ifq[0][i];
        jmp[j]++;
    }
    k=0;
    for( i=0;i<nq;i++ )
    {
        j=jmp[i];

        if(j!=0)
        {
            bsort(j,&ifq[1][k],&tmp[0]);
            permute(j,&(ifq[1][k]),&tmp[0]);

            for( ix=0;ix<nx;ix++ )
            {
                permute(j,&xc[ix][k],&tmp[0]);
            }

            for( ivar=0;ivar<(nx+1);ivar++ )
            {
                permute(j,&wnc[ivar][k],&tmp[0]);
            }

            permute(j,&wxdc[0][k],&tmp[0]);
        }
        k+=j;
    }

    // colour/reorder faces in order to remove dependencies at the
    // face end-points 
    // colour_lohner1(ifq,nfc,nq,VECLEN,fprm);
    colour_lohner2(ifq,nfc,nq,VECLEN,2*VECLEN,fprm);

    for( ivar=0;ivar<4;ivar++ )
    {
        permute(nfc,wnc[ivar],fprm);
    }
    for( ix=0;ix<nx;ix++ )
    {
        permute(nfc,xc[ix],fprm);
    }

    permute(nfc,wxdc[0],fprm);
    permute(nfc,ifq[0],fprm);
    permute(nfc,ifq[1],fprm);

    // cleanup
    for( i=0;i<nq;i++ )
    {
        delete[] ig[i];
        ig[i]= NULL;
    }
    delete[] lg[0];
    delete[] lg[1];
    delete[] ig;
    delete[] iwrk;
    delete[] fprm;
    delete[] jmp;

}


void rcmk(int n, int *lg[], int **ig, int *iprm, int *indx, int *iwrk)
{
    int ist,ien;
    int j,i,k,h;

    hsort(n,lg[1],indx);

    // initialize result and working array
    setv((int)0,n,(int)-99999,iprm);
    setv((int)0,n,(int)-1,iwrk);

    // initialize result array entry counter
    ien=0;

    do
    {

        ist= ien;
        // find first node with lowest degree
        // if it was not processed before
        for( j=0;j<n;j++ )
        {
            k= indx[j];
            if(iwrk[k] == -1)
            {
                // if not processed already
                // mark it and break
                i=k;
                break;
            }
        }
        // add it to our result array
        iprm[ist]= i;
        // set appropriate flag
        iwrk[i]= 0;
        // increment insertion counter
        ien= ist+1;

        // whilst we have nodes to process
        while(ien > ist)
        {

            for( j=ist;j<ien;j++ )
            {
                // take parent node
                i=iprm[j];
                // loop over its adjacent neighbours
                for( k=0;k<lg[0][i];k++ )
                {
                    h= ig[i][k];
                    // check neighbour has not been processed before
                    if(iwrk[h] == -1)
                    {
                        // set flag to process
                        iwrk[h]=-2;
                    }
                }
            }

            ist= ien;
            // loop through all nodes
            for( j=0;j<n;j++ )
            {
                i=indx[j];
                // check nodes to process
                if(iwrk[i] == -2)
                {
                    // add them to the result array
                    // and increment counter
                    iprm[ien]= i;
                    iwrk[i]=0;
                    ien++;
                }
            }
        }
    }
    while(ien < n);
    // reverse array to obtain RCMK
    reverse(n,iprm);
    // construct index array for new label of node i
    for( j=0;j<n;j++ )
    {
        i= iprm[j];
        indx[i]= j;
    }
}

void build_graph(int nnode, int nedge, int *is[], int *lg[], int **ig)
{

    int           i,j,k,h,l,i0,i1;

    // loop over all edges
    for( i=0;i<nedge;i++ )
    {
        // load left
        i0= is[0][i];
        // load right
        i1= is[1][i];
        assert(i0 != i1);

        for( int ii=0;ii<2;ii++ )
        {
            // current degree of i0
            l= lg[0][i0];
            // create node neighbour list for i0
            if(!(ig[i0]))
            {
                ig[i0]= new int[MAXNEIGH];
            };
            assert(l < MAXNEIGH);
            k= -1;
            // loop through all neighbours and check if already added or duplicate
            for( j=0;j<l;j++ )
            {
                if(ig[i0][j] == i1)
                {
                    k= j;
                    break;
                }
            }
            // new neighbour to add, increment counter, do swap
            if(k == -1)
            {
                ig[i0][l]= i1;
                (lg[0][i0])++;
                lg[1][i0]= lg[0][i0];
            }
            swap(&i0,&i1);
        }
    }
}
