# include <cmath>
# include <cstdlib>
# include <domain.h>
# include <sizes.h>
# include <utils.h>

/**
 * The two colouring/reordering routines listed below have been re-written by the authors in C/C++
 * using the F77 versions published in https://doi.org/10.1002/cnm.1160 by R. Lohner. Consequently,
 * any usage of these should most importantly cite the original source as these have been mostly
 * adapted for this application and test case.
 **/

void cDomain::colour_lohner1(int *ifq[], int nfc, int n, int veclen, int *iprm)
{
    int *ledge;
    int *lpoin;
    int *edpass;
    int nenew=0;        // initialize new edge counter
    int npass=0;        // initialize edge pass counter
    int nedg0=0;        // starting edge
    int nvecl=0;        // initialize vector length
    int mvecl=veclen;   // initialize max vector lengh
    int iedge;          // edge iterator
    int count=0;
    int nedge=nfc;
    int i0,i1;

    ledge=new int[nfc];
    lpoin=new int[n];

    setv(0,nfc,-1,ledge);   // initialize edge number array
    setv(0,n,0,lpoin);      // initialize point number array


    do
    {
        npass++;               // update pass counter
        nvecl=0;               // reset vector counter

        for( iedge=nedg0;iedge<nedge;iedge++ )
        {
            i0=ifq[0][iedge];
            i1=ifq[1][iedge];

            // edge has not been visited before
            if(ledge[iedge]==-1)
            {
                // point 0 has not been used in this pass
                if(lpoin[i0]!=npass)
                {
                    // point 1 has not been used in this pass
                    if(lpoin[i1]!=npass)
                    {
                        ledge[iedge]=nenew;
                        iprm[nenew]=iedge;
                        nenew++;
                        lpoin[i0]=npass;     // mark points
                        lpoin[i1]=npass;     // mark points
                        nvecl++;             // increment vector counter

                        if(nvecl==mvecl)    // reached desired vector count
                            break;
                    }
                }
            }
        }
        for( iedge=nedg0;iedge<nedge;iedge++ )
        {
            if(ledge[iedge]==-1)
            {
                break;
            }
        }

        nedg0=std::min(iedge,nedge);

    }
    while(nenew!=nedge);

    // free resources
    delete[] ledge;
    delete[] lpoin;
}

void cDomain::colour_lohner2(int *ifq[], int nedge, int n, int mvecl, int mxedl, int *iprm)
{
    int *ledge;         // edge array
    int *lpoin;         // vertices array
    int nenew;          // new edge counter
    int npass;          // pass counter
    int nedg0;          // starting edge
    int nvecl;          // current vector length
    int nedgl;          // local storange list length
    int ipoi1,ipoi2;    // vertices 1,2
    int ip1,ip2;        // vertices 1,2 in vector group
    int kpoi1;          // starting vertex 1 of vector group
    int kvecl;          // vector group flag
    int iedge;          // face index
    int nene0;          // face index storage
    int iedgl;          // face index
    int ijum1, ijum2;   // jumps in first and second indices
    int ijmi1, ijmi2;   // minimum jumps in first and second indices
    int ipmi1,ipmi2;    // vertices with minimum jumps
    int iminl;          // minimum face index
    int *leorl;         // indices of best quality faces
    int *ledgl;         // list of faces in vector search
    int *lqual;         // quality of faces in terms of second index minimum jumps

    // allocate storage
    lqual= new int[mxedl+1];
    ledgl= new int[mxedl+1];
    leorl=new int[mxedl+1];
    lpoin=new int[n];
    ledge=new int[nedge];
    setv(0,nedge,-1,ledge);    // set all edges to unvisited, i.e. -1
    setv(0,n,0,lpoin);         // set all points to 0, npass should start from 1

    nenew=0;   // number of new edges added
    npass=0;   // number of passes
    nedg0=0;   // starting edge/face

    do
    {
        nvecl=0;                                                        // reset current vector length
        nedgl=0;                                                        // reset face local storage list size
        ipoi1=ifq[0][nedg0];                                            // vertex 1
        ipoi2=ifq[1][nedg0];                                            // vertex 2
        nvecl++;
        ledge[nedg0]=nenew;                                             // save face
        iprm[nenew]=nedg0;                                              // save face permutation
        npass++;                                                        // update pass counter
        lpoin[ipoi1]=npass;                                             // mark vertices
        lpoin[ipoi2]=npass;
        nenew++;

        if(nedg0 != nedge)
        {
            kpoi1=ipoi1;
            kvecl=0;
            for( iedge=nedg0+1;iedge<nedge;iedge++ )
            {
                if(ledge[iedge] == -1)                                // face is unmarked
                {
                    ip1=ifq[0][iedge];
                    ip2=ifq[1][iedge];

                    if(lpoin[ip1] != npass)                           // vertex 1 unused
                    {
                        if(lpoin[ip2] != npass)                       // vertex 2 unused
                        {
                            if(ip1 > kpoi1)                           // new first index
                            {
                                if(kvecl==0)                          // new second index
                                {
                                    kpoi1=ip1;
                                    kvecl=1;
                                    ijum1=abs(ipoi1-ip1);
                                    ijum2=abs(ipoi2-ip2);
                                    iminl=iedge;
                                    ijmi1=ijum1;
                                    ijmi2=ijum2;
                                    ipmi1=ip1;
                                    ipmi2=ip2;
                                }
                                else
                                {
                                    ledgl[nedgl]=iminl;                // already compared
                                    lqual[nedgl]=ijmi2;                // save best quality face
                                    lpoin[ipmi1]=npass;
                                    lpoin[ipmi2]=npass;
                                    kvecl=0;
                                    nedgl++;
                                }
                            }
                            else
                            {
                                kvecl++;                               // point is known
                                ijum1=abs(ipoi1-ip1);
                                ijum2=abs(ipoi2-ip2);

                                if(ijum2 < ijmi2)
                                {
                                    iminl=iedge;
                                    ijmi1=ijum1;
                                    ijmi2=ijum2;
                                    ipmi1=ip1;
                                    ipmi2=ip2;
                                }
                            }
                        }
                    }
                }
                if(nedgl >= mxedl)                                 // group is big enough
                {
                    break;
                }
            }
        }

        if(nedgl != 0)
        {
            nvecl=std::min(mvecl,nedgl);                          // get nvecl worth of
            bsort(nedgl,lqual,leorl);                             // best quality faces
            nene0=nenew;
            for( iedgl=0;iedgl<nvecl-1;iedgl++ )
            {
                iedge=ledgl[leorl[iedgl]];                        // save them
                ledge[iedge]=nenew;
                iprm[nenew]=iedge;
                nenew++;
            }
        }

        for( iedge=nedg0;iedge<nedge;iedge++ )                    // look for next face
        {
            if(ledge[iedge] == -1)
            {
                break;
            }
        }

        nedg0=std::min(iedge,nedge);

    }
    while(nenew != nedge);

    // free resources
    delete[] ledge;
    delete[] lpoin;
    delete[] leorl;
    delete[] lqual;
    delete[] ledgl;
}
